package com.t3.happysmile.happysmilesapp.views;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;

public class BetterSRLayout extends SwipeRefreshLayout
{
    public BetterSRLayout( Context context )
    {
        super( context );
    }

    public BetterSRLayout( Context context, AttributeSet attrs )
    {
        super( context, attrs );
    }

    @Override
    protected Parcelable onSaveInstanceState()
    {
        Parcelable superState = super.onSaveInstanceState();
        return new SavedState( superState, this.isRefreshing() );
    }

    @Override
    protected void onRestoreInstanceState( Parcelable state )
    {
        SavedState savedState = ( SavedState ) state;
        super.onRestoreInstanceState( savedState.getSuperState() );
        this.setRefreshing( savedState.isRefreshing() );
    }

    protected static class SavedState extends BaseSavedState
    {
        private boolean isRefreshing;

        public SavedState( Parcel source )
        {
            super( source );
            this.isRefreshing = source.readInt() == 1;
        }

        public SavedState( Parcelable superState, boolean isRefreshing )
        {
            super( superState );
            this.isRefreshing = isRefreshing;
        }

        public boolean isRefreshing()
        {
            return this.isRefreshing;
        }

        @Override
        public void writeToParcel( Parcel dest, int flags )
        {
            super.writeToParcel( dest, flags );
            dest.writeInt( isRefreshing ? 1 : 0 );
        }

        public static final Creator<SavedState> CREATOR = new Creator<SavedState>()
        {
            @Override
            public SavedState createFromParcel( Parcel source )
            {
                return new SavedState( source );
            }

            @Override
            public SavedState[] newArray( int size )
            {
                return new SavedState[size];
            }
        };
    }
}
