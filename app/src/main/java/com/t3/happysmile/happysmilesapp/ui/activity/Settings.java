package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.t3.happysmile.happysmilesapp.R;


public class Settings extends Activity {

    ImageView menuList,back_image_setting;
    TextView trackBand,myprofile,setting,aboutus,happystore,upgrade;
    View view_ll;
    LinearLayout more_menu_layout;
    boolean more_menu=false;
    Bundle b;
    String classType,className;
    int page;
    static Boolean receive=false,sound=false,vibrate=false,appSounds=false;
    TextView receiveNotifications,soundsNotifications,vibrateNotifications,app_sound;
    ImageView receiveNotifications_image,soundsNotifications_image,vibrateNotifications_image,app_sound_image;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    String myClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        /*getActionBar().hide();*/
        b=getIntent().getExtras();
        pref=getApplication().getSharedPreferences("MyPref", 0);
        edit=pref.edit();
        receive=pref.getBoolean("receive",false);
        sound=pref.getBoolean("sound",false);
        vibrate=pref.getBoolean("vibrate",false);
        appSounds=pref.getBoolean("appSounds",false);
        classType=b.getString("class");
        className=b.getString("className");
        page=b.getInt("page");

        view_ll=(View)findViewById(R.id.view_ll_setting);
        more_menu_layout=(LinearLayout)findViewById(R.id.more_menu_layout_setting);
        back_image_setting=(ImageView)findViewById(R.id.back_image_setting);
        menuList=(ImageView)findViewById(R.id.menuList_setting);
        trackBand =(TextView)findViewById(R.id.trackBand_setting);
        myprofile=(TextView)findViewById(R.id.myProfile_setting);
        setting =(TextView)findViewById(R.id.settings_setting);
        aboutus=(TextView)findViewById(R.id.aboutUs_setting);
        happystore=(TextView)findViewById(R.id.happUser_setting);
        upgrade=(TextView)findViewById(R.id.upgrade_setting);

        receiveNotifications=(TextView)findViewById(R.id.receiveNotifications);
        soundsNotifications=(TextView)findViewById(R.id.soundsNotifications);
        vibrateNotifications=(TextView)findViewById(R.id.vibrateNotifications);
        app_sound=(TextView)findViewById(R.id.app_sound);

        receiveNotifications_image=(ImageView)findViewById(R.id.receiveNotifications_image);
        soundsNotifications_image=(ImageView)findViewById(R.id.soundsNotifications_image);
        vibrateNotifications_image=(ImageView)findViewById(R.id.vibrateNotifications_image);
        app_sound_image=(ImageView)findViewById(R.id.app_sound_image);
        if (receive==false) {
            receive=true;
            edit.putBoolean("receive",true);
            edit.commit();
            receiveNotifications.setTextColor(Color.parseColor("#595959"));
            receiveNotifications_image.setImageResource(R.drawable.checked);
        }else
        {
            receive=false;
            edit.putBoolean("receive",false);
            edit.commit();
            receiveNotifications.setTextColor(Color.parseColor("#A5A5A5"));
            receiveNotifications_image.setImageResource(R.drawable.unchecked);
        }



        if (sound==false) {
        sound=true;
        edit.putBoolean("sound",true);
        edit.commit();
        soundsNotifications.setTextColor(Color.parseColor("#595959"));
        soundsNotifications_image.setImageResource(R.drawable.checked);
        }else
        {
        sound=false;
        edit.putBoolean("sound",false);
        edit.commit();
        soundsNotifications.setTextColor(Color.parseColor("#A5A5A5"));
        soundsNotifications_image.setImageResource(R.drawable.unchecked);

        }

        if (vibrate==false) {
        vibrate=true;
        edit.putBoolean("vibrate",true);
        edit.commit();
        vibrateNotifications.setTextColor(Color.parseColor("#595959"));
        vibrateNotifications_image.setImageResource(R.drawable.checked);

        }else
        {
        vibrate=false;
        edit.putBoolean("vibrate",false);
        edit.commit();
        vibrateNotifications.setTextColor(Color.parseColor("#A5A5A5"));
        vibrateNotifications_image.setImageResource(R.drawable.unchecked);
        }

        if (appSounds==false) {
        appSounds=true;
        edit.putBoolean("appSounds",true);
        edit.commit();
        app_sound.setTextColor(Color.parseColor("#595959"));
        app_sound_image.setImageResource(R.drawable.checked);

        }else
        {
        appSounds=false;
        edit.putBoolean("appSounds",false);
        edit.commit();
        app_sound.setTextColor(Color.parseColor("#A5A5A5"));
        app_sound_image.setImageResource(R.drawable.unchecked);

        }

        receiveNotifications_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (receive==false) {
                    receive=true;
                    edit.putBoolean("receive",true);
                    edit.commit();
                    receiveNotifications.setTextColor(Color.parseColor("#595959"));
                    receiveNotifications_image.setImageResource(R.drawable.checked);
                }else
                {
                    receive=false;
                    edit.putBoolean("receive",false);
                    edit.commit();
                    receiveNotifications.setTextColor(Color.parseColor("#A5A5A5"));
                    receiveNotifications_image.setImageResource(R.drawable.unchecked);
                }
            }
        });
        soundsNotifications_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sound==false) {
                    sound=true;
                    edit.putBoolean("sound",true);
                    edit.commit();
                       soundsNotifications.setTextColor(Color.parseColor("#595959"));
                    soundsNotifications_image.setImageResource(R.drawable.checked);
                }else
                {
                    sound=false;
                    edit.putBoolean("sound",false);
                    edit.commit();
                    soundsNotifications.setTextColor(Color.parseColor("#A5A5A5"));
                    soundsNotifications_image.setImageResource(R.drawable.unchecked);

                }
            }
        });
        vibrateNotifications_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vibrate==false) {
                    vibrate=true;
                    edit.putBoolean("vibrate",true);
                    edit.commit();
                    vibrateNotifications.setTextColor(Color.parseColor("#595959"));
                    vibrateNotifications_image.setImageResource(R.drawable.checked);

                }else
                {
                    vibrate=false;
                    edit.putBoolean("vibrate",false);
                    edit.commit();
                    vibrateNotifications.setTextColor(Color.parseColor("#A5A5A5"));
                    vibrateNotifications_image.setImageResource(R.drawable.unchecked);
                }
            }
        });
        app_sound_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (appSounds==false) {
                    appSounds=true;
                    edit.putBoolean("appSounds",true);
                    edit.commit();
                    app_sound.setTextColor(Color.parseColor("#595959"));
                    app_sound_image.setImageResource(R.drawable.checked);

                }else
                {
                    appSounds=false;
                    edit.putBoolean("appSounds",false);
                    edit.commit();
                    app_sound.setTextColor(Color.parseColor("#A5A5A5"));
                    app_sound_image.setImageResource(R.drawable.unchecked);

                }
            }
        });
        back_image_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(classType.equalsIgnoreCase("fragment"))
                {
                    Intent backarrow=new Intent(Settings.this,Path.class);
                    backarrow.putExtra("currentPage",page);
                    startActivity(backarrow);
                }else
                {

                     myClass = "com.example.test.newapplication.ui.activites.activity."+className;
                    Class<?> myClass1 = null;
                    try {
                        myClass1 = Class.forName(myClass);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    try {
                        Activity obj = (Activity) myClass1.newInstance();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    Intent backarrow=new Intent(Settings.this,myClass1);
                    backarrow.putExtra("class", "activity");
                    backarrow.putExtra("className","Settings");
                    backarrow.putExtra("page", 1);
                    startActivity(backarrow);
                }
            }
        });

        view_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(more_menu==false)
                {

                }
                else
                {

                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu=false;
                    view_ll.setVisibility(View.INVISIBLE);

                }
            }
        });
        trackBand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                selectTaskIntent.putExtra("class", "fragment");
                selectTaskIntent.putExtra("className",myClass);
                selectTaskIntent.putExtra("page", page);
                startActivity(selectTaskIntent);

            }
        });
        myprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                Intent selectTaskIntent=new Intent(getApplicationContext(),MyProfile.class);
                selectTaskIntent.putExtra("class", "fragment");
                selectTaskIntent.putExtra("className",myClass);
                selectTaskIntent.putExtra("page", page);
                startActivity(selectTaskIntent);

            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                /*Intent selectTaskIntent=new Intent(getApplicationContext(),Settings.class);
                startActivity(selectTaskIntent);*/
            }
        });
        aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                /*Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });
        happystore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                /*Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });

        upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
               /* Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });

        menuList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (more_menu == false) {
                    more_menu_layout.setVisibility(View.VISIBLE);
                    view_ll.setVisibility(View.VISIBLE);
                    more_menu = true;
                } else {
                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu = false;
                    view_ll.setVisibility(View.INVISIBLE);
                }
            }
        });



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
