package com.t3.happysmile.happysmilesapp.Exception;

import android.content.Context;

import com.t3.happysmile.happysmilesapp.Dailog;

/**
 * Created by Shivaji on 05-01-2016.
 */
public class BaseException extends Exception {

    public static void run(Context context, String title, String Message){
        Dailog showAlertDialog = new Dailog();
        showAlertDialog.showAlertDialog(context,title,Message);
    }
}
