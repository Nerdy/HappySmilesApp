package com.t3.happysmile.happysmilesapp.model;

public class Items
{
    private String follows;

    private String id;

    private String leads;

    private String username;

    private String regTime;

    private String leaderCount;

    private String followerCount;

    public String getFollows ()
    {
        return follows;
    }

    public void setFollows (String follows)
    {
        this.follows = follows;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLeads ()
    {
        return leads;
    }

    public void setLeads (String leads)
    {
        this.leads = leads;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getRegTime ()
    {
        return regTime;
    }

    public void setRegTime (String regTime)
    {
        this.regTime = regTime;
    }

    public String getLeaderCount ()
    {
        return leaderCount;
    }

    public void setLeaderCount (String leaderCount)
    {
        this.leaderCount = leaderCount;
    }

    public String getFollowerCount ()
    {
        return followerCount;
    }

    public void setFollowerCount (String followerCount)
    {
        this.followerCount = followerCount;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [follows = "+follows+", id = "+id+", leads = "+leads+", username = "+username+", regTime = "+regTime+", leaderCount = "+leaderCount+", followerCount = "+followerCount+"]";
    }
}


