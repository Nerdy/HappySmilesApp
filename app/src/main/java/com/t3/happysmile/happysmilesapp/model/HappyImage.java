package com.t3.happysmile.happysmilesapp.model;


import android.os.Parcel;
import android.os.Parcelable;

public class HappyImage implements Parcelable
{
    HappyUser user;
    String id;


    GeoPt latlon;

    protected HappyImage(Parcel in) {
        user = in.readParcelable(HappyUser.class.getClassLoader());
        id = in.readString();
        latlon=in.readParcelable(GeoPt.class.getClassLoader());
    }

    public static final Creator<HappyImage> CREATOR = new Creator<HappyImage>() {
        @Override
        public HappyImage createFromParcel(Parcel in) {
            return new HappyImage(in);
        }

        @Override
        public HappyImage[] newArray(int size) {
            return new HappyImage[size];
        }
    };

    public HappyUser getUser() {
        return user;
    }

    public void setUser(HappyUser user) {
        this.user = user;
    }
    public GeoPt getLatlon() {
        return latlon;
    }

    public void setLatlon(GeoPt latlon) {
        this.latlon = latlon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(user, flags);
        dest.writeString(id);
    }
}
