package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.itwenty.passwordedittext.PasswordEditText;
import com.t3.happysmile.happysmilesapp.HappyUserApi;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.model.HappyUser;
import com.t3.happysmile.happysmilesapp.model.User;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class NewSignUpActivity extends Activity {
    TextView login_here_btn;
    Button nextbtn;
    Context context;
    boolean dataEntered;
    EditText usernameTv,emailTv;
    PasswordEditText passwordTv;
    String username ;//= "arun123";
    String email;//="arun123@gmail.com";
    String password;//="123arun123";
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sign_up);
        pref=getApplication().getSharedPreferences("MyPref", 0);
        edit=pref.edit();
        /*getActionBar().hide();*/
        login_here_btn=(TextView)findViewById(R.id.login_here_btn);
        nextbtn=(Button)findViewById(R.id.nextbtn);
        usernameTv= (EditText)findViewById(R.id.edt_signup_name);
        emailTv=(EditText)findViewById(R.id.edt_signup_email);
        passwordTv=(PasswordEditText)findViewById(R.id.edt_signup_password);
        username = String.valueOf(usernameTv.getText());//'"arun123";
        final StringBuffer name=new StringBuffer(usernameTv.getText());
        email=emailTv.getText().toString();//"arun123@gmail.com";
        password=passwordTv.getText().toString();//"123arun123";
        login_here_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginbtn = new Intent(NewSignUpActivity.this, LoginActivity.class);
                startActivity(loginbtn);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });

        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(getActivity(), LoginSignup.class);
                startActivity(intent);*/
                if(!usernameTv.getText().toString().isEmpty()) {
                    if ((Patterns.EMAIL_ADDRESS.matcher(emailTv.getText().toString()).matches())) {
                        if ((passwordTv.getText().toString()).length() >= 8)
                        {
                            RestAdapter restAdapter = new RestAdapter.Builder()
                                    .setEndpoint("https://happysmilesapi.appspot.com/api")
                                    .setLogLevel(RestAdapter.LogLevel.FULL)
                                    .build();
                            HappyUserApi apiService =
                                    restAdapter.create(HappyUserApi.class);
                            nextbtn.setText("Doing....");
                            apiService.sendDataRegistration(usernameTv.getText().toString(), emailTv.getText().toString(), passwordTv.getText().toString(), new Callback<User>() {
                                @Override
                                public void success(User user, Response response) {
                                    // Access user here after response is parsed

                                 //   Toast.makeText(getApplication(), "sucess==" + response, Toast.LENGTH_LONG).show();
                                    Log.d("ResponseSignup1", response.getReason());
                                    System.out.println("ResponseSignup2" + response.toString());
                                    try {
                                        String userId=user.getId();
                                        edit.putString("userId", userId);
                                        edit.commit();
                                          Intent loginbtn = new Intent(NewSignUpActivity.this, LoginActivity.class);
                                          startActivity(loginbtn);
                                          overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                     }
                                    catch (Exception e)
                                    {
                                        e.printStackTrace();
                                        System.out.println("Exceptioncatch"+e.toString());
                                    }

                                }

                                @Override
                                public void failure(RetrofitError retrofitError) {
                                    // Log error here since request failed
                                    Log.d("ResponseSignup1", retrofitError.toString());
                                    /*if(retrofitError.getResponse().getStatus()==400)
                                    {
                                        Toast.makeText(getApplicationContext(), "Invalid username,email,password"
                                                , Toast.LENGTH_LONG).show();


                                    }else if(retrofitError.getResponse().getStatus()==409)
                                    {
                                        Toast.makeText(getApplicationContext(), "username or email already exists"
                                                , Toast.LENGTH_LONG).show();

                                    }*/
                                }
                            });
                        }
                        else
                        {
                            passwordTv.setError("Password Should contain atleast 8 character");
                            return;
                        }

                    } else {
                        emailTv.setError("Enter Valid Email Address");
                        return;
                    }
                }else
                {
                    usernameTv.setError("Enter UserName");
                    return;
                }
                /*RequestInterceptor requestInterceptor = new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Authorization", "Retrofit-Sample-App");
                    }
                };
              RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint("https://happysmilesapi.appspot.com/api")
                       .setRequestInterceptor(requestInterceptor)
                      .setLogLevel(RestAdapter.LogLevel.FULL)
                      .build();
                HappyUser apiService =
                        restAdapter.create(HappyUser.class);

                apiService.sendDataRegistration(username, email, password,new Callback<User>() {
                    @Override
                    public void success(User user, Response response) {
                        // Access user here after response is parsed
                        Toast.makeText(getApplicationContext(),"sucess=="+response,Toast.LENGTH_LONG).show();
                        Log.d("Response",user.toString());
                        String name=user.getUsername();
                        Log.d("nameNew",user.getUsername());
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        // Log error here since request failed
                        Toast.makeText(getApplication(),"fail=="+retrofitError
                                 ,Toast.LENGTH_LONG).show();
                        Log.d("Response", retrofitError.toString());
                    }
                });
*/
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
