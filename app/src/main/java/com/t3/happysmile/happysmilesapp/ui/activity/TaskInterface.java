package com.t3.happysmile.happysmilesapp.ui.activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.adapter.IconPageIndicator;
import com.t3.happysmile.happysmilesapp.adapter.PageIndicator;
import com.t3.happysmile.happysmilesapp.adapter.TestFragmentAdapter;


public class TaskInterface extends Fragment {

    ImageView selectTask,band_icon,story_icon,camera,menuList,time_icon,imagetaskdescription,viewpager_cancel;
    ImageView taskTick,cameraTick,storyTick,qrcodeTick;
    TextView trackBand,myprofile,setting,aboutus,happystore,upgrade;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    LinearLayout ll_taskdone,more_menu_layout,ll_alltaskdone;
    RelativeLayout viewpager_bg;
    int subtaskNo;
    boolean more_menu=false;
    PopupWindow window;
    View view_ll;
    String subTaskCompletedBeforeTime;
    TestFragmentAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;
    public static final TaskInterface newInstance(String message)
    {
        TaskInterface f = new TaskInterface();
        Bundle bdl = new Bundle(1);
        bdl.putString("Data", message);
        f.setArguments(bdl);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_tast_interface, container, false);
        //setContentView(R.layout.activity_tast_interface);
        view_ll=(View)view.findViewById(R.id.view_ll);
        pref=getActivity().getSharedPreferences("MyPref", 0);
        edit=pref.edit();
        time_icon=(ImageView)view.findViewById(R.id.time_icon);
        selectTask=(ImageView)view.findViewById(R.id.selectTask);
        band_icon =(ImageView)view.findViewById(R.id.band_icon);
        story_icon=(ImageView)view.findViewById(R.id.story_icon);
        camera=(ImageView)view.findViewById(R.id.task_pic_icon);
        taskTick=(ImageView)view.findViewById(R.id.timeTick);
        cameraTick=(ImageView)view.findViewById(R.id.camera_tick);
        storyTick=(ImageView)view.findViewById(R.id.story_tick);
        qrcodeTick=(ImageView)view.findViewById(R.id.qrcode_tick);
        ll_taskdone=(LinearLayout)view.findViewById(R.id.ll_taskdone);
        ll_alltaskdone=(LinearLayout)view.findViewById(R.id.ll_alltaskdone);
        more_menu_layout=(LinearLayout)view.findViewById(R.id.more_menu_layout);
        menuList=(ImageView)view.findViewById(R.id.menuList);
        imagetaskdescription=(ImageView)view.findViewById(R.id.imagetaskdescription);
        trackBand =(TextView)view.findViewById(R.id.trackBand);
        myprofile=(TextView)view.findViewById(R.id.myProfile);
        setting =(TextView)view.findViewById(R.id.settings);
        aboutus=(TextView)view.findViewById(R.id.aboutUs);
        happystore=(TextView)view.findViewById(R.id.happUser);
        upgrade=(TextView)view.findViewById(R.id.upgrade);
        viewpager_bg=(RelativeLayout)view.findViewById(R.id.viewpager_bg);
        viewpager_cancel=(ImageView)view.findViewById(R.id.viewpager_cancel);
        mAdapter = new TestFragmentAdapter(getActivity().getSupportFragmentManager(),getActivity());
        mPager = (ViewPager)view.findViewById(R.id.pagertask);
        mPager.setAdapter(mAdapter);
        mIndicator = (IconPageIndicator)view.findViewById(R.id.indicatortask);
        mIndicator.setViewPager(mPager);
        subTaskCompletedBeforeTime=pref.getString("subTaskCompletedBeforeTime","");
        if(subTaskCompletedBeforeTime.equals("yes"))
        {
            ll_alltaskdone.setVisibility(View.VISIBLE);
            ll_taskdone.setVisibility(View.INVISIBLE);
            selectTask.setVisibility(View.INVISIBLE);

        }
        view_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (more_menu == false) {

                } else {

                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu = false;
                    view_ll.setVisibility(View.INVISIBLE);

                }
            }
        });
        trackBand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectTaskIntent = new Intent(getActivity(), MapsActivity.class);
                selectTaskIntent.putExtra("class", "fragment");
                selectTaskIntent.putExtra("className", "TaskInterface");
                selectTaskIntent.putExtra("page", 0);
                startActivity(selectTaskIntent);
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu = false;
                view_ll.setVisibility(View.INVISIBLE);
            }
        });
        myprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectTaskIntent = new Intent(getActivity(), MyProfile.class);
                selectTaskIntent.putExtra("class", "fragment");
                selectTaskIntent.putExtra("className", "TaskInterface");
                selectTaskIntent.putExtra("page", 0);
                startActivity(selectTaskIntent);
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu = false;
                view_ll.setVisibility(View.INVISIBLE);
            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu = false;
                view_ll.setVisibility(View.INVISIBLE);
                Intent selectTaskIntent = new Intent(getActivity(), Settings.class);
                selectTaskIntent.putExtra("class", "fragment");
                selectTaskIntent.putExtra("className", "TaskInterface");
                selectTaskIntent.putExtra("page", 0);
                startActivity(selectTaskIntent);

            }
        });
        aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
            }
        });
        happystore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
            }
        });

        upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
            }
        });


        subtaskNo=pref.getInt("subTask",1);
       /* view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(window.isShowing())
                {
                    window.dismiss();
                }
            }
        });*/

        imagetaskdescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewpager_bg.setVisibility(View.VISIBLE);
            }
        });
        viewpager_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewpager_bg.setVisibility(View.INVISIBLE);
                imagetaskdescription.setVisibility(View.INVISIBLE);
            }
        });
        if(subtaskNo==1)
        {

        }
        else if(subtaskNo==2)
        {
            cameraTick.setImageResource(R.drawable.tick_2);
            selectTask.setImageResource(R.drawable.story_edit_default_32);
        }
        else if(subtaskNo==3)
        {
            cameraTick.setImageResource(R.drawable.tick_2);
            storyTick.setImageResource(R.drawable.tick_2);
           // selectTask.setImageResource(R.drawable.ban_icon);
            selectTask.setVisibility(View.INVISIBLE);
            ll_taskdone.setVisibility(View.VISIBLE);
            ll_alltaskdone.setVisibility(View.INVISIBLE);
        }
        else if(subtaskNo==4)
        {
            cameraTick.setImageResource(R.drawable.tick_2);
            storyTick.setImageResource(R.drawable.tick_2);
            qrcodeTick.setImageResource(R.drawable.tick_2);
        }
        selectTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pref.getString("bandcode","")!=null){
                if(subtaskNo==1)
                {
                    //
                    Intent selectTaskIntent=new Intent(getActivity(),CameraActivity.class);
                    startActivity(selectTaskIntent);
                    // overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                }
                else if(subtaskNo==2)
                {
                    cameraTick.setImageResource(R.drawable.tick_2);
                    selectTask.setImageResource(R.drawable.story_edit_default_48);
                    Intent selectTaskIntent=new Intent(getActivity(),StoryActivity.class);
                    startActivity(selectTaskIntent);
                    //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                }
                else if(subtaskNo==3)
                {
                    cameraTick.setImageResource(R.drawable.tick_2);
                    storyTick.setImageResource(R.drawable.tick_2);

                    /*selectTask.setImageResource(R.drawable.ban_icon);
                    Intent selectTaskIntent=new Intent(getActivity(),ScanQRCode.class);
                    startActivity(selectTaskIntent);*/
                   // overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                }
                else if(subtaskNo==4)
                {
                    cameraTick.setImageResource(R.drawable.tick_2);
                    storyTick.setImageResource(R.drawable.tick_2);
                    qrcodeTick.setImageResource(R.drawable.tick_2);
                }

            }else{
                    Toast.makeText(getActivity(),"Please Scan the band First",Toast.LENGTH_SHORT).show();
                }
            }
        });
        time_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pref.getString("bandcode","")!=null) {

                    //put this code there when 24 hour is completed
                    edit.putString("ScanBand", "yes");
                    edit.commit();
                }else{
                    Toast.makeText(getActivity(),"Please Scan the band First",Toast.LENGTH_SHORT).show();
                }
               /* Intent selectTaskIntent=new Intent(getActivity(),VideoActivity.class);
                //Intent selectTaskIntent=new Intent(getActivity(),Path.class);
                startActivity(selectTaskIntent);
               // overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
*/            }
        });
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pref.getString("bandcode","")!=null) {
                    Intent selectTaskIntent = new Intent(getActivity(), CameraActivity.class);
                    startActivity(selectTaskIntent);
                    // overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }else{
                    Toast.makeText(getActivity(),"Please Scan the band First",Toast.LENGTH_SHORT).show();
                }
            }
        });
        band_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent selectTaskIntent=new Intent(getActivity(),ScanQRCode.class);
                startActivity(selectTaskIntent);*/
                //overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

               //put is code there when task is completed before 24 hour
                if(pref.getString("bandcode","")!=null) {
                edit.putInt("subTask", 4);
                edit.putString("subTaskCompletedBeforeTime", "yes");
                edit.commit();
                }else{
                    Toast.makeText(getActivity(),"Please Scan the band First",Toast.LENGTH_SHORT).show();
                }
            }
        });
        story_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pref.getString("bandcode","")!=null) {

                Intent storyScreen =new Intent(getActivity(),StoryActivity.class);
                startActivity(storyScreen);
               // overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }else{
                Toast.makeText(getActivity(),"Please Scan the band First",Toast.LENGTH_SHORT).show();
            }
            }
        });




        menuList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(more_menu==false)
                {
                    more_menu_layout.setVisibility(View.VISIBLE);
                    view_ll.setVisibility(View.VISIBLE);
                    more_menu=true;
                }else
                {
                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu=false;
                    view_ll.setVisibility(View.INVISIBLE);
                }

            }
        });

        return view;
    }



}
