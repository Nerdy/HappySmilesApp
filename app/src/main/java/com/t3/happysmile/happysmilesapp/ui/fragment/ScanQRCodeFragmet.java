package com.t3.happysmile.happysmilesapp.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.adapter.GPSTracker;
import com.t3.happysmile.happysmilesapp.ui.activity.CameraActivity;
import com.t3.happysmile.happysmilesapp.ui.activity.StoryActivity;
import com.t3.happysmile.happysmilesapp.ui.activity.TaskDescription;

import java.io.File;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the

 * to handle interaction events.
 * Use the {@link ScanQRCodeFragmet#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ScanQRCodeFragmet extends Fragment implements QRCodeReaderView.OnQRCodeReadListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    double latitude ;
    double longitude ;
    private TextView myTextView;
    private QRCodeReaderView mydecoderview;
    public ImageView line_image;
    Bundle b=new Bundle();
    GPSTracker gps;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    CameraActivity cam;
    StoryActivity storyActivity;
    String story,band;
    File image;
    String authKey;
     int taskNo;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.

     * @return A new instance of fragment ScanQRCodeFragmet.
     */
    // TODO: Rename and change types and number of parameters
    public static ScanQRCodeFragmet newInstance(String param1) {
        ScanQRCodeFragmet fragment = new ScanQRCodeFragmet();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public ScanQRCodeFragmet() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_scan_qrcode_fragmet, container, false);
        pref=getActivity().getSharedPreferences("MyPref", 0);
        edit=pref.edit();
        mydecoderview = (QRCodeReaderView) view.findViewById(R.id.qrdecoderviewfragment);
        mydecoderview.setOnQRCodeReadListener(this);

        line_image = (ImageView) view.findViewById(R.id.red_line_image_fragment);
        TranslateAnimation mAnimation = new TranslateAnimation(
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.ABSOLUTE, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                TranslateAnimation.RELATIVE_TO_PARENT, 0.5f);
        mAnimation.setDuration(1000);
        mAnimation.setRepeatCount(-1);
        mAnimation.setRepeatMode(Animation.REVERSE);
        mAnimation.setInterpolator(new LinearInterpolator());
        line_image.setAnimation(mAnimation);
        return view;
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
//            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    // Called when a QR is decoded
    // "text" : the text encoded in QR
    // "points" : points where QR control points are placed
    @Override
    public void onQRCodeRead(String text, PointF[] points) {
        band=text;
        taskNo=taskNo+pref.getInt("Task",0);
        edit.putString("band", band);
        edit.putInt("Task", taskNo);
        edit.putString("ScanBand","no");
        edit.commit();


//        TimeService task=new TimeService();
     //   task.onCreate();
        // create class object
        gps = new GPSTracker(getActivity());
        // check if GPS enabled
        if(gps.canGetLocation()){
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            // \n is for new line
            Toast.makeText(getActivity(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
            Intent i=new Intent(getActivity(),TaskDescription.class);
            startActivity(i);
          //  overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        }else{
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gps.showSettingsAlert();
        }

    }

    @Override
    public void cameraNotFound() {

    }

    @Override
    public void QRCodeNotFoundOnCamImage() {

    }



    @Override
 public void onResume() {
        super.onResume();
        mydecoderview.getCameraManager().startPreview();
    }

    @Override
    public void onPause() {
        super.onPause();
        mydecoderview.getCameraManager().stopPreview();
    }
}
