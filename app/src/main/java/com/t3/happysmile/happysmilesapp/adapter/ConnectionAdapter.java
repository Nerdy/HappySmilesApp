package com.t3.happysmile.happysmilesapp.adapter;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.model.Items;
import com.t3.happysmile.happysmilesapp.ui.activity.ImageConnections;

import java.util.ArrayList;
import java.util.Locale;

public class ConnectionAdapter extends ArrayAdapter {
    static ArrayList<Items> Itemscon;
    ArrayList<Items> arraylist;
    LayoutInflater inflater;
    Context context;
    ViewHolder holder;
    public ConnectionAdapter(FragmentActivity activity, ArrayList<Items> itemscon) {
        super(activity, R.layout.connection_list_item, itemscon);
        this.context=activity;
        this.Itemscon=itemscon;
        /*this.arraylist = new ArrayList<Items>();
        this.arraylist.addAll(itemscon);
*/

    }


    @Override
    public int getCount() {
        holder =new ViewHolder();
        return Itemscon.size();


    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
public class ViewHolder
{
    TextView usernameTv;
    ImageView ping,profile;

}
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView;

        if (convertView == null) {
            inflater = (LayoutInflater)context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            rowView = new View(context);
        } else {
            rowView = (View) convertView;
        }
        rowView = inflater.inflate(R.layout.connection_list_item, parent,false);
        holder.usernameTv=(TextView) rowView.findViewById(R.id.connectionUsername);
        holder.ping=(ImageView) rowView.findViewById(R.id.ping_item);
        holder.profile=(ImageView) rowView.findViewById(R.id.Iv_profle_connection);

        Log.d("AdapterData", " " + Itemscon.get(position).getUsername());
        holder.usernameTv.setText(Itemscon.get(position).getUsername());
        holder.ping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Ping", Toast.LENGTH_SHORT).show();
            }
        });
        holder.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context, ImageConnections.class);
                i.putExtra("id",Itemscon.get(position).getId());
                i.putExtra("username",Itemscon .get(position).getUsername());
                context.startActivity(i);

            }
        });
        holder.usernameTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(context, ImageConnections.class);
                i.putExtra("id",Itemscon.get(position).getId());
                i.putExtra("username",Itemscon.get(position).getUsername());
                context.startActivity(i);

            }
        });
        return rowView;
    }
    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        Itemscon.clear();
        if (charText.length() == 0) {
            Itemscon.addAll(Itemscon);
        }
        else
        {
            for (Items wp : Itemscon)
            {
                if (wp.getUsername().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    Itemscon.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
