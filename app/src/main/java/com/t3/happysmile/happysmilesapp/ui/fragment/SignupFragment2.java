package com.t3.happysmile.happysmilesapp.ui.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.t3.happysmile.happysmilesapp.Dailog;
import com.t3.happysmile.happysmilesapp.HappyUserApi;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.model.User;
import com.t3.happysmile.happysmilesapp.ui.activity.CupAnimationFrist;
import com.t3.happysmile.happysmilesapp.ui.activity.LoginActivity;
import com.t3.happysmile.happysmilesapp.ui.activity.SignUpActivity;

import java.io.File;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/*
*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SignupFragment2.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SignupFragment2#newInstance} factory method to
 * create an instance of this fragment.
*/
public class SignupFragment2 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ImageView Iv_profle_signup;
    EditText contactNo;
    TextView login_here_btn;
    Button done_btn;
    Context context;
    SignupFragment1 frag;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    String userName,email,password;
    Dailog d;
   /* private OnFragmentInteractionListener mListener;*/

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentSignupFragment2.
     */
    // TODO: Rename and change types and number of parameters
    public static SignupFragment2 newInstance(String param1, String param2) {
        SignupFragment2 fragment = new SignupFragment2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SignupFragment2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_signup_fragment2, container, false);
           frag=new SignupFragment1();
        Iv_profle_signup=(ImageView)view.findViewById(R.id.Iv_profle_signup);
        contactNo=(EditText)view.findViewById(R.id.contactNo);
        login_here_btn=(TextView)view.findViewById(R.id.login_here_btn2);
        done_btn=(Button)view.findViewById(R.id.done_btn);
        pref=getActivity().getSharedPreferences("MyPref", 0);
        edit=pref.edit();

        userName=pref.getString("signUpUsername","");
        email=pref.getString("SignUpEmail","");
        password=pref.getString("SignUpPassword","");
        d=new Dailog();
        Iv_profle_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage(1);
            }
        });
        login_here_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), LoginActivity.class);
               startActivity(intent);

            }
        });
        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            //  Log.d("Data=", String.valueOf(frag.emailTv.getText()));
               /* Intent intent2 = new Intent(getActivity(), LoginSignup.class);
                startActivity(intent2);*/
                if(!userName.equals("")) {
                    if ((Patterns.EMAIL_ADDRESS.matcher(email).matches())) {
                        if (password.length() >= 8)
                        {
                    RestAdapter restAdapter = new RestAdapter.Builder()
                            .setEndpoint("https://happysmilesapi.appspot.com/api")
                            .setLogLevel(RestAdapter.LogLevel.FULL)
                            .build();
                    HappyUserApi apiService =
                            restAdapter.create(HappyUserApi.class);

                    apiService.sendDataRegistration(userName, email, password, new Callback<User>() {
                        @Override
                        public void success(User user, Response response) {
                            // Access user here after response is parsed

                            //   Toast.makeText(getApplication(), "sucess==" + response, Toast.LENGTH_LONG).show();
                            Log.d("ResponseSignup1", response.getReason());
                            System.out.println("ResponseSignup2" + response.toString());
                            try {


                                Intent login=new Intent(getActivity(),CupAnimationFrist.class);
                                startActivity(login);
                                ((Activity) getActivity()).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                            }
                            catch (Exception e)
                            {
                                e.printStackTrace();
                                System.out.println("Exceptioncatch"+e.toString());
                            }

                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {
                            // Log error here since request failed
                            Log.d("ResponseSignup1", retrofitError.toString());
                                    /*if(retrofitError.getResponse().getStatus()==400)
                                    {
                                        Toast.makeText(getApplicationContext(), "Invalid username,email,password"
                                                , Toast.LENGTH_LONG).show();


                                    }else if(retrofitError.getResponse().getStatus()==409)
                                    {
                                        Toast.makeText(getApplicationContext(), "username or email already exists"
                                                , Toast.LENGTH_LONG).show();

                                    }*/
                        }
                    });
                        }
                        else
                        {
                            //passwordTv.setError("Password Should contain atleast 8 character");
                            d.showAlertDialog(getActivity(),"Enter Password","Password Should contain atleast 8 character");
                            return;
                        }

                    } else {
                       // emailTv.setError("Enter Valid Email Address");
                        d.showAlertDialog(getActivity(),"Enter Email","Enter Valid Email Address");

                        return;
                    }
                }else
                {
                   // usernameTv.setError("Enter UserName");
                    d.showAlertDialog(getActivity(),"Enter UserName","Please Enter Usernames");

                    return;
                }

            }
        });

        return view;
    }

    private void selectImage(final int code) {
        final CharSequence[] options = { "Take Photo", "Choose from Gallery",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                                    .getPath());
                    startActivityForResult(intent, 1 * 10 + code);
                } else if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Picture"),
                            2 * 10 + code);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @SuppressWarnings("deprecation")
    public String getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor =  getActivity().getContentResolver().query(uri, projection, null, null,
                null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }

    @Override
    public  void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK)
        {

            // /////for first image.//////
            if (requestCode == 11)
            {
                if (data != null)
                {

                    String[] projection = {MediaStore.Images.Media.DATA};
                    Cursor cursor =  getActivity().getContentResolver().query(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                            projection, null, null, null);
                    int column_index_data = cursor
                            .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                    cursor.moveToLast();

                    String imagePath = cursor.getString(column_index_data);


                    // btnphotobrowse.setText("selected");
                    File imgFile = new File(imagePath);
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                            .getAbsolutePath());
                    Bitmap bm = Bitmap.createScaledBitmap(myBitmap, 500, 600,
                            true);
                    Iv_profle_signup.setImageBitmap(bm);
                }
            }
            else if (requestCode == 21)
            {

                Uri selectedImageUri = data.getData();
                String capturedImage = getPath(selectedImageUri);
                 Toast.makeText(getActivity(), "Image...." + capturedImage,
                         Toast.LENGTH_LONG).show();
                Log.i("image", "" + capturedImage);
                System.out.println("Captured Image..........");
                File imgFile = new File(capturedImage);

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
                        .getAbsolutePath());
                Bitmap bm = Bitmap.createScaledBitmap(myBitmap, 500, 600, true);
                Iv_profle_signup.setImageBitmap(bm);

            }
            else

            {

            }
        }

    }

   /* // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    *//**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     *//*
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
*/
}
