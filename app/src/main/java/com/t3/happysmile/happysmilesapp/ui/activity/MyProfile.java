package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.t3.happysmile.happysmilesapp.R;


public class MyProfile extends Activity {

    ImageView menuList,back_image;
    TextView trackBand,myprofile,setting,aboutus,happystore,upgrade,nameTv_myprofile;
    View view_ll;
    LinearLayout more_menu_layout;
    boolean more_menu=false;
    Bundle b;
    String classType,className;
    int page;
    String myClass;
    LinearLayout edtNameLL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        b=getIntent().getExtras();
        /*getActionBar().hide();*/
        classType=b.getString("class");
        className=b.getString("className");
        page=b.getInt("page");
        edtNameLL=(LinearLayout)findViewById(R.id.edtNameLL);
        view_ll=(View)findViewById(R.id.view_ll_profile);
        more_menu_layout=(LinearLayout)findViewById(R.id.more_menu_layout_profile);
        back_image=(ImageView)findViewById(R.id.back_image);
        menuList=(ImageView)findViewById(R.id.menuList_profile);
        trackBand =(TextView)findViewById(R.id.trackBand_profile);
        myprofile=(TextView)findViewById(R.id.myProfile_profile);
        setting =(TextView)findViewById(R.id.settings_profile);
        aboutus=(TextView)findViewById(R.id.aboutUs_profile);
        happystore=(TextView)findViewById(R.id.happUser_profile);
        upgrade=(TextView)findViewById(R.id.upgrade_profile);
        nameTv_myprofile=(TextView)findViewById(R.id.nameTv_myprofile);
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(classType.equalsIgnoreCase("fragment"))
                {
                Intent backarrow=new Intent(MyProfile.this,Path.class);
                    backarrow.putExtra("currentPage",page);
                    startActivity(backarrow);
                }else
                {
                     myClass = "com.example.test.newapplication.ui.activites.activity."+className;
                    Class<?> myClass1 = null;
                    try {
                        myClass1 = Class.forName(myClass);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    try {
                        Activity obj = (Activity) myClass1.newInstance();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    Intent backarrow=new Intent(MyProfile.this,myClass1);
                    backarrow.putExtra("class", "activity");
                    backarrow.putExtra("className","MyProfile");
                    backarrow.putExtra("page", 1);
                    startActivity(backarrow);
                }
            }
        });
        edtNameLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*
                Intent i=new Intent(MyProfile.this,EditProfileName.class);
                i.putExtra("name",nameTv_myprofile.getText().toString());
                i.putExtra("class", classType);
                i.putExtra("className",className);
                i.putExtra("page", page);
                startActivity(i);
                */

            }
        });
        view_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(more_menu==false)
                {

                }
                else
                {

                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu=false;
                    view_ll.setVisibility(View.INVISIBLE);

                }
            }
        });
        trackBand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                selectTaskIntent.putExtra("class", "activity");
                selectTaskIntent.putExtra("className",myClass);
                selectTaskIntent.putExtra("page", page);
                startActivity(selectTaskIntent);

            }
        });
        myprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);

            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                Intent selectTaskIntent=new Intent(getApplicationContext(),Settings.class);
                selectTaskIntent.putExtra("class", "activity");
                selectTaskIntent.putExtra("className",myClass);
                selectTaskIntent.putExtra("page", page);
                startActivity(selectTaskIntent);
            }
        });
        aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                /*Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });
        happystore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                /*Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });

        upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
               /* Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });

        menuList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (more_menu == false) {
                    more_menu_layout.setVisibility(View.VISIBLE);
                    view_ll.setVisibility(View.VISIBLE);
                    more_menu = true;
                } else {
                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu = false;
                    view_ll.setVisibility(View.INVISIBLE);
                }
            }
        });



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
