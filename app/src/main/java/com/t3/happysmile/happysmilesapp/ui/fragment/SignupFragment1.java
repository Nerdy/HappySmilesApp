package com.t3.happysmile.happysmilesapp.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.t3.happysmile.happysmilesapp.HappyUserApi;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.model.HappyUser;
import com.t3.happysmile.happysmilesapp.model.User;
import com.t3.happysmile.happysmilesapp.ui.activity.LoginActivity;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/*
*
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SignupFragment1.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SignupFragment1#newInstance} factory method to
 * create an instance of this fragment.
*/
public class SignupFragment1 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    TextView login_here_btn;
    Button nextbtn;
    Context context;
    boolean dataEntered;
    EditText usernameTv,emailTv,passwordTv;
    String username ;//= "arun123";
    String email;//="arun123@gmail.com";
    String password;//="123arun123";
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SignupFragment1.
     */
    // TODO: Rename and change types and number of parameters
    public static SignupFragment1 newInstance(String param1, String param2) {
        SignupFragment1 fragment = new SignupFragment1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SignupFragment1() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_signup_fragment1, container, false);
        login_here_btn=(TextView)view.findViewById(R.id.login_here_btn);
        nextbtn=(Button)view.findViewById(R.id.nextbtn);
        usernameTv= (EditText)view.findViewById(R.id.edt_signup_name);
        emailTv=(EditText)view.findViewById(R.id.edt_signup_email);
        passwordTv=(EditText)view.findViewById(R.id.edt_signup_password);
         /*username = String.valueOf(usernameTv.getText());//'"arun123";*/
       final StringBuffer name=null;//=new StringBuffer(usernameTv.getText());
     //   email=emailTv.getText().toString();//"arun123@gmail.com";
      //  password=passwordTv.getText().toString();//"123arun123";
        pref=getActivity().getSharedPreferences("MyPref", 0);
        edit=pref.edit();
        usernameTv.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)

               //usernameTv.setText("");
                edit.putString("signUpUsername",usernameTv.getText().toString());
                edit.commit();
            }
        });
        emailTv.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)

                    //usernameTv.setText("");
                    edit.putString("SignUpEmail",emailTv.getText().toString());
                edit.commit();
            }
        });
        passwordTv.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(s.length() != 0)

                    //usernameTv.setText("");
                    edit.putString("SignUpPassword",passwordTv.getText().toString());
                edit.commit();
            }
        });
          nextbtn.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  {

               /* Intent intent = new Intent(getActivity(), LoginSignup.class);
                startActivity(intent);*/
                      if(!usernameTv.getText().toString().isEmpty()) {
                          if ((Patterns.EMAIL_ADDRESS.matcher(emailTv.getText().toString()).matches())) {
                              if ((passwordTv.getText().toString()).length() >= 8)
                              {
                                  edit.putString("signUpUsername",usernameTv.getText().toString());
                                  edit.putString("SignUpEmail",emailTv.getText().toString());
                                  edit.putString("SignUpPassword",passwordTv.getText().toString());
                                  edit.commit();


                                  SignupFragment2 fragment2 = new SignupFragment2();
                                  FragmentManager fragmentManager = getFragmentManager();
                                  FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                  fragmentTransaction.replace(R.id.signup_fragment1, fragment2);
                                  fragmentTransaction.commit();
                                            /*  Intent loginbtn = new Intent(getActivity(), LoginActivity.class);
                                              startActivity(loginbtn);*/
                                  ((Activity) getActivity()).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);


                              }
                              else
                              {
                                  passwordTv.setError("Password Should contain atleast 8 character");
                                  return;
                              }

                          } else {
                              emailTv.setError("Enter Valid Email Address");
                              return;
                          }
                      }else
                      {
                          usernameTv.setError("Enter UserName");
                          return;
                      }
                  }


              }
          });
        login_here_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginbtn = new Intent(getActivity(), LoginActivity.class);
                startActivity(loginbtn);
                ((Activity) getActivity()).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });

       /* login_here_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               *//* Intent intent = new Intent(getActivity(), LoginSignup.class);
                startActivity(intent);*//*
                if((Patterns.EMAIL_ADDRESS.matcher(emailTv.getText().toString()).matches()))
                {
                    if((passwordTv.getText().toString()).length()>=1)
                    {

                    }
                    else
                    {
                        passwordTv.setError("Password Should contain atleast 8 character");
                        return;
                    }

                }
                else
                {
                    emailTv.setError("Enter Valid Email Address");
                    return;
                }
                Log.d("Register data==","value=="+username+password+email+"name"+name);*/
               /* RequestInterceptor requestInterceptor = new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Authorization", "Retrofit-Sample-App");
                    }
                };*/
              /*  RestAdapter restAdapter = new RestAdapter.Builder()
                        .setEndpoint("https://happysmilesapi.appspot.com/api")
                        *//*.setRequestInterceptor(requestInterceptor)*//*
                        .build();
                HappyUser apiService =
                        restAdapter.create(HappyUser.class);

                apiService.sendDataRegistration(username, email, password,new Callback<User>() {
                    @Override
                    public void success(User user, Response response) {
                        // Access user here after response is parsed
                        Toast.makeText(getActivity(),"sucess=="+response,Toast.LENGTH_LONG).show();
                        Log.d("Response",response.toString());
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        // Log error here since request failed
                        Toast.makeText(getActivity(),"fail=="+retrofitError
                                 ,Toast.LENGTH_LONG).show();
                        Log.d("Response", retrofitError.toString());
                    }
                });*/

//            }
//        });



        return view;
    }


}
