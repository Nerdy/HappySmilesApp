package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.t3.happysmile.happysmilesapp.R;

public class EditProfileName extends Activity {

    ImageView menuList,back_image;
    TextView trackBand,myprofile,setting,aboutus,happystore,upgrade,nameTv_myprofile;
    View view_ll;
    LinearLayout more_menu_layout;
    boolean more_menu=false;
    Bundle b;
    String classType,className;
    int page;
    String myClass;
    EditText edtname_profile;
    TextWatcher tw;
    Button savebtn;
    String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_name);
/*
        getActionBar().hide();
*/
        b=getIntent().getExtras();
        name=b.getString("name");
        classType=b.getString("class");
        className=b.getString("className");
        page=b.getInt("page");
        edtname_profile=(EditText)findViewById(R.id.edtname_profile);
        view_ll=(View)findViewById(R.id.view_ll_profile);
        more_menu_layout=(LinearLayout)findViewById(R.id.more_menu_layout_profile);
        back_image=(ImageView)findViewById(R.id.back_image_name);
        menuList=(ImageView)findViewById(R.id.menuList_profile_name);
        trackBand =(TextView)findViewById(R.id.trackBand_profilename);
        myprofile=(TextView)findViewById(R.id.myProfile_profilename);
        setting =(TextView)findViewById(R.id.settings_profilename);
        aboutus=(TextView)findViewById(R.id.aboutUs_profilename);
        happystore=(TextView)findViewById(R.id.happUser_profilename);
        upgrade=(TextView)findViewById(R.id.upgrade_profilename);
        savebtn=(Button)findViewById(R.id.savebtn);
        edtname_profile.setText(name);
        back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent backarrow=new Intent(EditProfileName.this,MyProfile.class);
                    backarrow.putExtra("class", classType);
                    backarrow.putExtra("className",className);
                    backarrow.putExtra("page", page);
                    startActivity(backarrow);
            }
        });
        tw=new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                edtname_profile.setTextColor(Color.parseColor("#585858"));
            }
            @Override
            public void afterTextChanged(Editable s) {
               if(savebtn.getVisibility()==View.INVISIBLE)
               {
                   savebtn.setVisibility(View.VISIBLE);
               }else{

               }
            }};
        edtname_profile.addTextChangedListener(tw);
        savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        view_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(more_menu==false)
                {

                }
                else
                {

                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu=false;
                    view_ll.setVisibility(View.INVISIBLE);

                }
            }
        });
        trackBand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                selectTaskIntent.putExtra("class", "activity");
                selectTaskIntent.putExtra("className",myClass);
                selectTaskIntent.putExtra("page", page);
                startActivity(selectTaskIntent);

            }
        });
        myprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);

            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                Intent selectTaskIntent=new Intent(getApplicationContext(),Settings.class);
                selectTaskIntent.putExtra("class", "activity");
                selectTaskIntent.putExtra("className",myClass);
                selectTaskIntent.putExtra("page", page);
                startActivity(selectTaskIntent);
            }
        });
        aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                /*Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });
        happystore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                /*Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });

        upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
               /* Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });

        menuList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (more_menu == false) {
                    more_menu_layout.setVisibility(View.VISIBLE);
                    view_ll.setVisibility(View.VISIBLE);
                    more_menu = true;
                } else {
                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu = false;
                    view_ll.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_profile_name, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
