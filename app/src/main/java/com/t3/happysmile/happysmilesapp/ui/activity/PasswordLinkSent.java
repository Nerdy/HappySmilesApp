package com.t3.happysmile.happysmilesapp.ui.activity;

import android.os.Bundle;
import android.app.Activity;

import com.t3.happysmile.happysmilesapp.R;

public class PasswordLinkSent extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_link_sent);
    }

}
