package com.t3.happysmile.happysmilesapp.model.ModelConnectionImage;

public class Items
{
    private String id;

    private String servingUrl;

    private String story;

    private String timeStamp;

    private Location location;

    private User user;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getServingUrl ()
    {
        return servingUrl;
    }

    public void setServingUrl (String servingUrl)
    {
        this.servingUrl = servingUrl;
    }

    public String getStory ()
    {
        return story;
    }

    public void setStory (String story)
    {
        this.story = story;
    }

    public String getTimeStamp ()
    {
        return timeStamp;
    }

    public void setTimeStamp (String timeStamp)
    {
        this.timeStamp = timeStamp;
    }

    public Location getLocation ()
    {
        return location;
    }

    public void setLocation (Location location)
    {
        this.location = location;
    }

    public User getUser ()
    {
        return user;
    }

    public void setUser (User user)
    {
        this.user = user;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", servingUrl = "+servingUrl+", story = "+story+", timeStamp = "+timeStamp+", location = "+location+", user = "+user+"]";
    }
}


