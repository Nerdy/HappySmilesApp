package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.t3.happysmile.happysmilesapp.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CameraVideoActivity extends Activity implements SurfaceHolder.Callback,
        View.OnClickListener {

    private SurfaceView surfaceView;
    private Camera camera;
    private ImageView flipCamera;
    private ImageView flashCameraButton,story_camera,share_cancel;
    private ImageView captureImage,friendimagecamera;
    private int cameraId;
    private boolean flashmode = false;
    private int rotation;
    static boolean click=true;
    static boolean ok=false;
    static boolean switchCmaera=true;
    static boolean shareImage=false;
    Bitmap loadedImage = null;
    Bitmap rotatedBitmap = null;
    LinearLayout share_ll;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    public File imageFile;
    boolean recording;
    private MediaRecorder mediaRecorder;
    Button myButton;
    private SurfaceHolder surfaceHolder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_demo);
        // camera surface view created
        pref=getApplication().getSharedPreferences("MyPref",0);
        edit=pref.edit();
        cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
        flipCamera = (ImageView) findViewById(R.id.flipCameraVideo);
        flashCameraButton = (ImageView) findViewById(R.id.story_cameraVideo);
        captureImage = (ImageView) findViewById(R.id.captureImageVideo);
        surfaceView = (SurfaceView) findViewById(R.id.surfaceViewVideo);
        friendimagecamera=(ImageView)findViewById(R.id.friendimagecamera);
        share_ll=(LinearLayout)findViewById(R.id.share_llVideo);
        share_cancel=(ImageView)findViewById(R.id.share_cancelVideo);
//        surfaceHolder = surfaceView.getHolder();
//        surfaceHolder.addCallback(this);
        flipCamera.setOnClickListener(this);
        captureImage.setOnClickListener(this);
        flashCameraButton.setOnClickListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (Camera.getNumberOfCameras() > 1) {
            flipCamera.setVisibility(View.VISIBLE);
        }
        /*if (!getBaseContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_FLASH)) {
            flashCameraButton.setVisibility(View.GONE);
        }*/
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (!openCamera(Camera.CameraInfo.CAMERA_FACING_BACK)) {
            alertCameraDialog();
        }

    }

    private boolean openCamera(int id) {
        boolean result = false;
        cameraId = id;
        releaseCamera();
        try {
            camera = Camera.open(cameraId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (camera != null) {
            try {
                setUpCamera(camera);

                camera.setErrorCallback(new Camera.ErrorCallback() {

                    @Override
                    public void onError(int error, Camera camera) {

                    }
                });
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
                result = true;
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
                releaseCamera();
            }
        }
        return result;
    }

    private void setUpCamera(Camera c) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degree = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degree = 0;
                break;
            case Surface.ROTATION_90:
                degree = 90;
                break;
            case Surface.ROTATION_180:
                degree = 180;
                break;
            case Surface.ROTATION_270:
                degree = 270;
                break;

            default:
                break;
        }

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            // frontFacing
            rotation = (info.orientation + degree) % 330;
            rotation = (360 - rotation) % 360;
        } else {
            // Back-facing
            rotation = (info.orientation - degree + 360) % 360;
        }
        c.setDisplayOrientation(rotation);
        Camera.Parameters params = c.getParameters();

        showFlashButton(params);

        List<String> focusModes = params.getSupportedFlashModes();
        if (focusModes != null) {
            if (focusModes
                    .contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                params.setFlashMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
        }

        params.setRotation(rotation);
    }

    private void showFlashButton(Camera.Parameters params) {
        boolean showFlash = (getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_FLASH) && params.getFlashMode() != null)
                && params.getSupportedFlashModes() != null
                && params.getSupportedFocusModes().size() > 1;

        flashCameraButton.setVisibility(showFlash ? View.VISIBLE
                : View.INVISIBLE);

    }

    private void releaseCamera() {
        try {
            if (camera != null) {
                camera.setPreviewCallback(null);
                camera.setErrorCallback(null);
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error", e.toString());
            camera = null;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.story_camera:
//                flashOnButton();
                if(shareImage) {
                    Intent story = new Intent(CameraVideoActivity.this, StoryActivity.class);
                    startActivity(story);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    releaseCamera();
                }
                else
                {
                    Toast.makeText(getApplication(), "first take pic", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.flipCamera:
                if(switchCmaera)
                {
                    flipCamera();
                }
                else
                {
                    if(shareImage)
                    {
                        share_ll.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case R.id.captureImage:
                if(click)
                {
                   /// takeImage();
                    try{
                        if(recording){
                            // stop recording and release camera
                            mediaRecorder.stop();  // stop the recording
                            releaseMediaRecorder(); // release the MediaRecorder object

                            //Exit after saved
                            //finish();
                          //  myButton.setText("REC");
                            recording = false;
                        }else{

                            //Release Camera before MediaRecorder start
                            releaseCamera();

                            if(!prepareMediaRecorder()){
                                Toast.makeText(CameraVideoActivity.this,
                                        "Fail in prepareMediaRecorder()!\n - Ended -",
                                        Toast.LENGTH_LONG).show();
                                finish();
                            }

                            mediaRecorder.start();
                            recording = true;
                           // myButton.setText("STOP");
                        }
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
                else
                {
                    if(ok)
                    {
                        //send image to server and move to other page
                        Intent task=new Intent(CameraVideoActivity.this,TaskInterface.class);
                        startActivity(task);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        releaseCamera();
                    }
                    else
                    {

                    }

                }

                break;

            case R.id.share_cancel:
                share_ll.setVisibility(View.INVISIBLE);
                break ;
            default:
                break;
        }
    }

    private void takeImage() {
        click=false;


        camera.takePicture(null, null, new Camera.PictureCallback() {



            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                try {
                    // convert byte array into bitmap

                    loadedImage = BitmapFactory.decodeByteArray(data, 0,
                            data.length);

                    // rotate Image
                    Matrix rotateMatrix = new Matrix();
                    rotateMatrix.postRotate(rotation);
                    rotatedBitmap = Bitmap.createBitmap(loadedImage, 0, 0,
                            loadedImage.getWidth(), loadedImage.getHeight(),
                            rotateMatrix, false);
                    String state = Environment.getExternalStorageState();
                    File folder = null;
                    if (state.contains(Environment.MEDIA_MOUNTED)) {
                        folder = new File(Environment
                                .getExternalStorageDirectory() + "/100HappySmilesApp");
                    } else {
                        folder = new File(Environment
                                .getExternalStorageDirectory() + "/100HappySmilesApp");
                    }

                    boolean success = true;
                    if (!folder.exists()) {
                        success = folder.mkdirs();
                    }
                    if (success) {
                        java.util.Date date = new java.util.Date();
                        imageFile = new File(folder.getAbsolutePath()
                                + File.separator
                                + new Timestamp(date.getTime()).toString()
                                + "Image.jpg");

                        imageFile.createNewFile();

                        releaseCamera();
                    } else {
                        Toast.makeText(getBaseContext(), "Image Not saved",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    ByteArrayOutputStream ostream = new ByteArrayOutputStream();

                    // save image into gallery
                    rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);

                    FileOutputStream fout = new FileOutputStream(imageFile);
                    fout.write(ostream.toByteArray());
                    fout.close();
                    ContentValues values = new ContentValues();

                    values.put(MediaStore.Images.Media.DATE_TAKEN,
                            System.currentTimeMillis());
                    values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
                    values.put(MediaStore.MediaColumns.DATA,
                            imageFile.getAbsolutePath());

                    CameraVideoActivity.this.getContentResolver().insert(
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    friendimagecamera.setVisibility(View.VISIBLE);
                    flashCameraButton.setVisibility(View.VISIBLE);
                    captureImage.setImageResource(R.drawable.done_normal);
                    flipCamera.setImageResource(R.drawable.share_normal);
                    ok=true;
                    shareImage=true;
                    switchCmaera=false;
                    edit.putInt("subTask",2);
                    edit.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void flipCamera() {
        int id = (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK ? Camera.CameraInfo.CAMERA_FACING_FRONT
                : Camera.CameraInfo.CAMERA_FACING_BACK);
        if (!openCamera(id)) {
            alertCameraDialog();
        }
    }

    private void alertCameraDialog() {
        AlertDialog.Builder dialog = createAlert(CameraVideoActivity.this,"Camera info", "error to open camera");
        dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        dialog.show();
    }

    private AlertDialog.Builder createAlert(Context context, String title, String message) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(
                new ContextThemeWrapper(context,
                        android.R.style.Theme_Holo_Light_Dialog));
        dialog.setIcon(R.drawable.ic_launcher);
        if (title != null)
            dialog.setTitle(title);
        else
            dialog.setTitle("Information");
        dialog.setMessage(message);
        dialog.setCancelable(false);
        return dialog;

    }
    private void releaseMediaRecorder(){
        if (mediaRecorder != null) {
            mediaRecorder.reset();   // clear recorder configuration
            mediaRecorder.release(); // release the recorder object
            mediaRecorder = new MediaRecorder();
            camera.lock();           // lock camera for later use
        }
    }
    private boolean prepareMediaRecorder(){
        click=false;
        camera = getCameraInstance();
        mediaRecorder = new MediaRecorder();

        camera.unlock();
        mediaRecorder.setCamera(camera);

        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        mediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));

        mediaRecorder.setOutputFile("/sdcard/" + getFileName_CustomFormat() + ".mp4");
        //mediaRecorder.setOutputFile("/sdcard/myvideo1.mp4");
        mediaRecorder.setMaxDuration(60000); // Set max duration 60 sec.
        mediaRecorder.setMaxFileSize(50000000); // Set max file size 50M

        mediaRecorder.setPreviewDisplay(surfaceView.getHolder().getSurface());
        friendimagecamera.setVisibility(View.VISIBLE);
        flashCameraButton.setVisibility(View.VISIBLE);
        captureImage.setImageResource(R.drawable.done_normal);
        flipCamera.setImageResource(R.drawable.share_normal);
        ok=true;
        shareImage=true;
        switchCmaera=false;

        try {
            mediaRecorder.prepare();
        } catch (IllegalStateException e) {
            releaseMediaRecorder();
            return false;
        } catch (IOException e) {
            releaseMediaRecorder();
            return false;
        }
        return true;

    }
    private Camera getCameraInstance(){
        // TODO Auto-generated method stub
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    private String getFileName_CustomFormat() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH_mm_ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    private void flashOnButton() {
        /*if (camera != null) {
            try {
                Parameters param = camera.getParameters();
                param.setFlashMode(!flashmode ? Parameters.FLASH_MODE_TORCH
                        : Parameters.FLASH_MODE_OFF);
                camera.setParameters(param);
                flashmode = !flashmode;
            } catch (Exception e) {
                // TODO: handle exception
            }

        }*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent back=new Intent(CameraVideoActivity.this,TaskInterface.class);
        startActivity(back);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        releaseCamera();
    }
}