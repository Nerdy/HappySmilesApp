package com.t3.happysmile.happysmilesapp.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import com.t3.happysmile.happysmilesapp.FontCache;


public class CustomFontEditText extends EditText
{
    public CustomFontEditText( Context context )
    {
        super( context );
        init( context, null, 0 );
    }

    public CustomFontEditText( Context context, AttributeSet attrs )
    {
        super( context, attrs );
        init( context, attrs, 0 );
    }

    public CustomFontEditText( Context context, AttributeSet attrs, int defStyleAttr )
    {
        super( context, attrs, defStyleAttr );
        init( context, attrs, defStyleAttr );
    }

    private void init( Context context, AttributeSet attrs, int defStyleAttr )
    {
        FontCache.applyToView(this, attrs);
    }
}
