package com.t3.happysmile.happysmilesapp.model;

/**
 * Created by Arun on 01-10-2015.
 */
public class AccessToken {
    String access_token;


    String token_type;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }
    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

}
