package com.t3.happysmile.happysmilesapp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;

import com.t3.happysmile.happysmilesapp.R;

public class EnterEmailId extends Activity {

    Button submitBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_email_id);
        submitBtn=(Button)findViewById(R.id.submitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(EnterEmailId.this,EnterPassword.class);
                startActivity(i);

            }
        });

    }

}
