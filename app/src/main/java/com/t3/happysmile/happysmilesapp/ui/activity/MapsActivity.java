package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.t3.happysmile.happysmilesapp.R;

public class MapsActivity   extends Activity {
    private GoogleMap mMap;
    static boolean Iscamera = false;
    private static final LatLng DAVAO = new LatLng(7.0722, 125.6131);
    ImageView menuList,back_image_map;
    TextView trackBand,myprofile,setting,aboutus,happystore,upgrade;
    View view_ll;
    LinearLayout more_menu_layout;
    boolean more_menu=false;
    Bundle b;
    String classType,className;
    int page;
    String myClass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);
        b=getIntent().getExtras();
        classType=b.getString("class");
        className=b.getString("className");
        page=b.getInt("page");
//        setUpMapIfNeeded();
        /*getActionBar().hide();*/
        view_ll=(View)findViewById(R.id.view_ll_map);
        more_menu_layout=(LinearLayout)findViewById(R.id.more_menu_layout_map);
        back_image_map=(ImageView)findViewById(R.id.back_image_map);
        menuList=(ImageView)findViewById(R.id.menuList_map);
        trackBand =(TextView)findViewById(R.id.trackBand_map);
        myprofile=(TextView)findViewById(R.id.myProfile_map);
        setting =(TextView)findViewById(R.id.settings_map);
        aboutus=(TextView)findViewById(R.id.aboutUs_map);
        happystore=(TextView)findViewById(R.id.happUser_map);
        upgrade=(TextView)findViewById(R.id.upgrade_map);

        back_image_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(classType.equalsIgnoreCase("fragment"))
                {
                    Intent backarrow=new Intent(MapsActivity.this,Path.class);
                    backarrow.putExtra("currentPage",page);
                    startActivity(backarrow);
                }else
                {

                     myClass = "com.example.test.newapplication.ui.activites.activity."+className;
                    Class<?> myClass1 = null;
                    try {
                        myClass1 = Class.forName(myClass);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    try {
                        Activity obj = (Activity) myClass1.newInstance();
                    } catch (InstantiationException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    Intent backarrow=new Intent(MapsActivity.this,myClass1);
                    backarrow.putExtra("class", "activity");
                    backarrow.putExtra("className","MapsActivity");
                    backarrow.putExtra("page", page);
                    startActivity(backarrow);
                }
            }
        });


        view_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(more_menu==false)
                {

                }
                else
                {

                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu=false;
                    view_ll.setVisibility(View.INVISIBLE);

                }
            }
        });
        trackBand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);


            }
        });
        myprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                Intent selectTaskIntent=new Intent(getApplicationContext(),MyProfile.class);
                selectTaskIntent.putExtra("class", "fragment");
                selectTaskIntent.putExtra("className",myClass);
                selectTaskIntent.putExtra("page", page);
                startActivity(selectTaskIntent);

            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                Intent selectTaskIntent=new Intent(getApplicationContext(),Settings.class);
                selectTaskIntent.putExtra("class", "fragment");
                selectTaskIntent.putExtra("className",myClass);
                selectTaskIntent.putExtra("page", page);
                startActivity(selectTaskIntent);
            }
        });
        aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                /*Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });
        happystore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
                /*Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });

        upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
               /* Intent selectTaskIntent=new Intent(getApplicationContext(),MapsActivity.class);
                startActivity(selectTaskIntent);*/
            }
        });

        menuList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (more_menu == false) {
                    more_menu_layout.setVisibility(View.VISIBLE);
                    view_ll.setVisibility(View.VISIBLE);
                    more_menu = true;
                } else {
                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu = false;
                    view_ll.setVisibility(View.INVISIBLE);
                }
            }
        });



    }



    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();

    }


    // animate the zoom process

    private void setUpMapIfNeeded() {

        if (mMap == null) {

            // Try to obtain the map from the SupportMapFragment.
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();


        }
            mMap.setMyLocationEnabled(true);

            if (mMap != null) {

                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getMaxZoomLevel();
                mMap.getMinZoomLevel();
                mMap.getUiSettings();
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
                mMap.animateCamera(CameraUpdateFactory.zoomOut());
                mMap.animateCamera(CameraUpdateFactory.zoomTo(20));

                mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                    @Override
                    public void onMyLocationChange(Location arg0) {
                        // TODO Auto-generated method stub

                        mMap.clear();
                        mMap.addMarker(new MarkerOptions()
                                .position(
                                        new LatLng(arg0.getLatitude(), arg0
                                                .getLongitude()))
                                .title("I am Here!!")
                                .icon(BitmapDescriptorFactory
                                        .fromResource(R.drawable.excel)));
                        if (!Iscamera) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(arg0.getLatitude(), arg0
                                            .getLongitude()), 14));


                            Iscamera = true;
                        }
                        try {


                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(19.3022065,72.830934))

                                    .icon(BitmapDescriptorFactory
                                            .fromResource(R.drawable.excl)));


                            mMap.addMarker(new MarkerOptions()
                                    .position(
                                            new LatLng(19.2305985,72.8142935))
                                    .icon(BitmapDescriptorFactory
                                            .fromResource(R.drawable.excl)));

                            mMap.addMarker(new MarkerOptions()
                                    .position(
                                            new LatLng(19.1483657,72.7993085))

                                    .icon(BitmapDescriptorFactory
                                            .fromResource(R.drawable.excl)));


                            mMap.addMarker(new MarkerOptions()
                                    .position(
                                            new LatLng(19.1740417,72.735818))

                                    .icon(BitmapDescriptorFactory
                                            .fromResource(R.drawable.excl)));





                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(),
                                    e.getMessage(), Toast.LENGTH_LONG).show();
                            Log.e(e.getClass().getName(), e.getMessage(), e);
                        }

                    }
                });

            }
        }



    // ********************************************************************************************
    @SuppressWarnings("unused")
    private void setUpMap() {

        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title(
                "Marker"));
    }
}























































       /*

        Activity implements GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener, GoogleMap.OnMarkerDragListener {

    private AlphaAnimation alfaAnim;

    Intent intent;
    private GoogleMap googleMap, mMap;
    private TextView textviewDistanceDuration;
    static double latitude;
    static	double longitude;
    String vehicleId,carId;
    ArrayList<Latlonglist> latlongList;
    *//**
     * Latitude & Longitude of Current Position
     *//*
    LatLng latLngCurrent;
    static LatLng[] data;
    private LatLng latLngFirst,newlatlong;
    private SharedPreferences prefs,preferences;
    int lang;
    String userId;



    MapView map;

    @SuppressWarnings("null")
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        FragmentActivity activity = (FragmentActivity) getApplicationContext();
        SupportMapFragment fm = (SupportMapFragment)activity.getSupportFragmentManager()
                .findFragmentById(R.id.map);

        alfaAnim = new AlphaAnimation(1.0F, 0.8F);

        SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "UserID", MODE_PRIVATE);
        userId = pref.getString("userID", "");
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        lang = preferences.getInt("Langauge", 1);
        Intent in = getIntent();
        Bundle b = in.getExtras();
        latlongList = new ArrayList<Latlonglist>();





        int status = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(getBaseContext());

        ConnectivityManager conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        State mobile = conMan.getNetworkInfo(0).getState();
        State wifi = conMan.getNetworkInfo(1).getState();

        if (mobile == NetworkInfo.State.CONNECTED
                || mobile == NetworkInfo.State.CONNECTING
                || wifi == NetworkInfo.State.CONNECTED
                || wifi == NetworkInfo.State.CONNECTING) {
            if (status == ConnectionResult.SUCCESS) {

                // 19.2821659,72.8765366

                GPSTracker gpsTracker = new GPSTracker(this);

                // Current Latitude & Longitude

                Double stringLatitude = latitude;// gpsTracker.latitude;
                Double stringLongitude = longitude;// gpsTracker.longitude;

                latLngCurrent = new LatLng(latitude, longitude);// (gpsTracker.latitude,gpsTracker.longitude);

                stringLatitude = stringLatitude + 0.01;
                stringLongitude = stringLongitude + 0.10;

                latLngFirst = new LatLng(stringLatitude, stringLongitude);

                stringLatitude = stringLatitude + 0.10;
                stringLongitude = stringLongitude + 0.01;


                // textviewDistanceDuration = (TextView)
                // findViewById(R.id.tv_distance_time);

                googleMap = fm.getMap();
                googleMap.setMyLocationEnabled(true);

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLngCurrent).zoom(15).build();
                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));

                Marker markerCurrent = googleMap.addMarker(new MarkerOptions()
                        .position(latLngCurrent)
                        .title("Me")

                        .draggable(true));



                googleMap.setOnMarkerDragListener(this);

                googleMap.setOnMapClickListener(this);

                googleMap.setOnMarkerDragListener(this);
            } else {
                Toast.makeText(getApplicationContext(),
                        "Google Play Services Not Available",
                        Toast.LENGTH_SHORT).show();
                int requestCode = 10;
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status,
                        this, requestCode);
                dialog.show();
            }
        } else {
            Toast.makeText(getApplicationContext(),
                    "Check Internet Connection", Toast.LENGTH_SHORT).show();

        }


        new getnearTaxi().execute();
    }



    class getnearTaxi extends AsyncTask<String, Void, ArrayList<Latlonglist>> {
        ProgressDialog progressDialog;
        String msg;

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(MapsActivity.this, "Wait",
                    "Downloading...");

        }

        @Override
        protected ArrayList<Latlonglist> doInBackground(String... params) {

            String url = "http://demo1.geniesoftsystem.com/taxi_app/index.php/api/getNearTaxis?data=";
            JSONObject obj = new JSONObject();
            try {

                obj.put("userid", userId);
                obj.put("latitude", latitude);
                obj.put("longitude", longitude);
                System.out.println("latitude" + latitude + "longitude"
                        + longitude);
                JSONObject resObj = RestJsonClient.connect(url
                        + URLEncoder.encode(obj.toString(), "utf-8"));

                // get required data for booking details......

                System.out.println("Respose.........." + resObj);

                if (resObj.getString("response").equals("success")) {

                    JSONArray jsonArray = resObj.getJSONArray("userdata");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        Latlonglist list = new Latlonglist();

                        list.setDriverId(jsonObject.getString("driver_id"));
                        list.setLatitude(jsonObject.getDouble("latitude"));
                        list.setLongitude(jsonObject.getDouble("longitude"));

                        latlongList.add(list);

                    }
                    return latlongList;
                }
            } catch (JSONException e) {
                e.printStackTrace();
                msg = "No Internet";
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            return latlongList;
        }

        @SuppressWarnings("null")
        @Override
        protected void onPostExecute(ArrayList<Latlonglist> result) {
            progressDialog.dismiss();

            for (int j = 0; j < latlongList.size(); j++) {
                System.out.println("helooooooooooooooooooooo"+latlongList.get(j).getDriverId());
                System.out.println(latlongList.get(j).getLatitude());
                System.out.println(latlongList.get(j).getLongitude());
            }

            try {

                for (  int i = 0; i < latlongList.size(); i++) {

                    double lati = latlongList.get(i).getLatitude();
                    double longLat = latlongList.get(i).getLongitude();

                    googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lati, longLat))
                            .title("Driver"+latlongList.get(i).getDriverId())
                                  .draggable(true));

                    googleMap.setOnMarkerClickListener(new OnMarkerClickListener() {

                        @Override
                        public boolean onMarkerClick(Marker arg0) {

                            // TODO Auto-generated method stub
                            String str[]=(arg0.getTitle()).split("Driver");



                            for (  int k = 0; k < latlongList.size(); k++) {

*//*
                                Intent intent = new Intent(
                                        getApplicationContext(),
                                        DriverProfile.class);
                                intent.putExtra("driverId",str[1]);//latlongList.get(k).getDriverId());
                                intent.putExtra("latUSer",latitude);
                                intent.putExtra("longUSer",longitude);

                                startActivity(intent);*//*
                            }
                            return true;

                        }

                    });
                }




            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }

    @SuppressWarnings("unused")
    private String getDirectionsUrl(LatLng origin, LatLng destination) {

        // Origin of route
        String stringOrigin = "origin=" + origin.latitude + ","
                + origin.longitude;

        // CurrentPosition of route
        String stringDestination = "destination=" + destination.latitude + ","
                + destination.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = stringOrigin + "&" + stringDestination + "&"
                + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/"
                + output + "?" + parameters;

        return url;
    }

    *//** A method to download json data from url *//*
    private String downloadUrl(String stringURL) throws IOException {
        String data = new String();
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;
        try {

            // Creating an http connection to communicate with url
            httpURLConnection = (HttpURLConnection) new URL(stringURL)
                    .openConnection();

            // Connecting to url
            httpURLConnection.connect();

            // Reading data from url
            inputStream = httpURLConnection.getInputStream();

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream));

            StringBuffer stringBuffer = new StringBuffer();

            String string = new String();
            while ((string = bufferedReader.readLine()) != null) {
                stringBuffer.append(string);
            }
            data = stringBuffer.toString();
            bufferedReader.close();

        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            inputStream.close();
            httpURLConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String string = new String();
            try {
                // Fetching the data from web service
                string = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return string;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // Invokes the thread for parsing the JSON data
            new ParserTask().execute(result);
        }
    }

    *//** A class to parse the Google Places in JSON format *//*
    private class ParserTask extends
            AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(
                String... jsonData) {

            JSONObject jsonObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jsonObject = new JSONObject(jsonData[0]);
               *//* DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jsonObject);*//*
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions polylineOptions = null;

			*//*
			 * String for Distance from source to destination
			 *//*
            String distance = new String();

			*//*
			 * String for require time from source to destination
			 *//*
            String duration = new String();

            if (result.size() < 1) {
                Toast.makeText(MapsActivity.this, "No Points",
                        Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                polylineOptions = new PolylineOptions();
                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);
                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) { // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                polylineOptions.addAll(points);
                polylineOptions.width(5);
                polylineOptions.color(Color.parseColor("#3BB9FF"));
            }

            // Set Distance & Time in textviewDistanceDuration
            textviewDistanceDuration.setText("Distance:" + distance
                    + ", Duration:" + duration);

            // Drawing polyline in the Google googleMap for the i-th route
            googleMap.addPolyline(polylineOptions);
        }
    }

    private class GPSTracker extends Service implements LocationListener {
        private final Context mContext;

        // flag for GPS Status
        boolean isGPSEnabled = false;

        // flag for network status
        boolean isNetworkEnabled = false;

        private Location location;

        // The minimum distance to change updates in metters
        private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 10
        // metters

        // The minimum time between updates in milliseconds
        private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

        // Declaring a Location Manager
        protected LocationManager locationManager;

        public GPSTracker(Context context) {
            this.mContext = context;
            getLocation();
        }

        public Location getLocation() {
            try {
                locationManager = (LocationManager) mContext
                        .getSystemService(LOCATION_SERVICE);

                // getting GPS status
                isGPSEnabled = locationManager
                        .isProviderEnabled(LocationManager.GPS_PROVIDER);

                // getting network status
                isNetworkEnabled = locationManager
                        .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

                if (!isGPSEnabled && !isNetworkEnabled) {
                    // no network provider is enabled
                } else {

                    // First get location from Network Provider
                    if (isNetworkEnabled) {
                        locationManager.requestLocationUpdates(
                                LocationManager.NETWORK_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        Log.d("Network", "Network");

                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                            updateGPSCoordinates();
                        }
                    }

                    // if GPS Enabled get lat/long using GPS Services
                    if (isGPSEnabled) {
                        if (location == null) {
                            locationManager.requestLocationUpdates(
                                    LocationManager.GPS_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                            Log.d("GPS Enabled", "GPS Enabled");

                            if (locationManager != null) {
                                location = locationManager
                                        .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                                updateGPSCoordinates();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.e("Error : Location",
                        "Impossible to connect to LocationManager", e);
            }

            return location;
        }

        public void updateGPSCoordinates() {
            if (location != null) {
                // latitude = 33.979246;//location.getLatitude();
                // longitude = 35.630258;//location.getLongitude();
                latitude = location.getLatitude();
                longitude = location.getLongitude();

            }
        }

        @Override
        public void onLocationChanged(Location location) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public IBinder onBind(Intent intent) {
            return null;
        }
    }

    @Override
    public void onMarkerDrag(Marker arg0) {
        // TODO Auto-generated method stub

    }
    @Override
    public void onMarkerDragEnd(Marker arg0) {
        // TODO Auto-generated method stub
        Toast.makeText(getApplicationContext(),"End....Marker " + arg0.getId() + " Drag@" + arg0.getPosition(),
                Toast.LENGTH_SHORT).show();

        System.out.println(arg0.getPosition().latitude+""+arg0.getPosition().longitude);
        latitude=arg0.getPosition().latitude;
        longitude=arg0.getPosition().longitude;
        new getnearTaxi().execute();
    }
    @Override
    public void onMarkerDragStart(Marker arg0) {
        // TODO Auto-generated method stub
        Toast.makeText(getApplicationContext(),"start........"+
                        arg0.getId() + ", " + arg0.getId(),
                Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onMapClick(LatLng point) {
        // TODO Auto-generated method stub

        Toast.makeText(getApplicationContext(),
                point.latitude + ", " + point.longitude,
                Toast.LENGTH_SHORT).show();


    }
    @Override
    public void onMapLongClick(LatLng point) {
        // TODO Auto-generated method stub
        Toast.makeText(getApplicationContext(),"New marker added@" + point.toString(), Toast.LENGTH_SHORT).show();
        googleMap.addMarker(new MarkerOptions()
                .position(point)
                .draggable(true));

    }
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
      *//*  Intent i=new Intent(RequestTaxi.this,DashboardActivity.class);
        startActivity(i);*//*
    }
}
*/