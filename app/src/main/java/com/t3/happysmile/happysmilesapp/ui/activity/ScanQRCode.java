package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PointF;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.t3.happysmile.happysmilesapp.HappyUserApi;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.adapter.GPSTracker;
import com.t3.happysmile.happysmilesapp.model.HappyImage;

import java.io.File;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ScanQRCode extends Activity implements QRCodeReaderView.OnQRCodeReadListener,LocationListener {

    double latitude ;
    double longitude ;
        private TextView myTextView;
        private QRCodeReaderView mydecoderview;
        public ImageView line_image;
        Bundle b=new Bundle();
        GPSTracker gps;
        SharedPreferences pref;
        SharedPreferences.Editor edit;
    CameraActivity cam;
    StoryActivity storyActivity;
    String story,band;
    File image;
    String authKey;
    Button shipBtn;
           @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
               pref=getApplication().getSharedPreferences("MyPref", 0);
               edit=pref.edit();
              setContentView(R.layout.activity_scan_qrcode);
              cam=new CameraActivity();
              storyActivity=new StoryActivity();
               image=cam.imageFile;
               story=storyActivity.story;
            mydecoderview = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
            mydecoderview.setOnQRCodeReadListener(this);
/*
               getActionBar().hide();
*/
            myTextView = (TextView) findViewById(R.id.exampleTextView);
               shipBtn=(Button)findViewById(R.id.shipBtn);
            line_image = (ImageView) findViewById(R.id.red_line_image);


            TranslateAnimation mAnimation = new TranslateAnimation(
                    TranslateAnimation.ABSOLUTE, 0f,
                    TranslateAnimation.ABSOLUTE, 0f,
                    TranslateAnimation.RELATIVE_TO_PARENT, 0f,
                    TranslateAnimation.RELATIVE_TO_PARENT, 0.5f);
            mAnimation.setDuration(1000);
            mAnimation.setRepeatCount(-1);
            mAnimation.setRepeatMode(Animation.REVERSE);
            mAnimation.setInterpolator(new LinearInterpolator());
            line_image.setAnimation(mAnimation);
               shipBtn.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                     Intent magicalintent = new Intent(
                               ScanQRCode.this,
                               Path.class);
                       magicalintent.putExtra("currentPage",1);
                       startActivity(magicalintent);
                       finish();
                   }
               });
        }


        // Called when a QR is decoded
        // "text" : the text encoded in QR
        // "points" : points where QR control points are placed
        @Override
        public void onQRCodeRead(String text, PointF[] points) {

            //myTextView.setText(text);
           // band=text;
            edit.putInt("Task",1);

            edit.commit();
          //  sendData();

            // create class object
            gps = new GPSTracker(ScanQRCode.this);
            // check if GPS enabled
            if(gps.canGetLocation()){
                 latitude = gps.getLatitude();
                 longitude = gps.getLongitude();
                 edit.putString("bandcode",text);
                 edit.putString("lat", String.valueOf(latitude));
                 edit.putString("log", String.valueOf(longitude));
                 edit.putBoolean("login",true);
                 edit.putInt("subTask", 0);

                 edit.commit();
                // \n is for new line
                ///Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
                Intent i=new Intent(ScanQRCode.this,TaskDescription.class);

                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }else{
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gps.showSettingsAlert();
            }

        }





    // Called when your device have no camera
        @Override
        public void cameraNotFound() {

        }

        // Called when there's no QR codes in the camera preview image
        @Override
        public void QRCodeNotFoundOnCamImage() {

        }

        @Override
        protected void onResume() {
            super.onResume();
            mydecoderview.getCameraManager().startPreview();
        }

        @Override
        protected void onPause() {
            super.onPause();
            mydecoderview.getCameraManager().stopPreview();
        }
    public void sendData()
    {
        //=pref.getString("authKey","");
        String username=pref.getString("username","");
        String password=pref.getString("password","");

        String lat= String.valueOf(latitude);
        String lon= String.valueOf(longitude);

        final String credentials = username + ":" + password;
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                authKey = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                request.addHeader("Authorization", authKey);
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://happysmilesapi.appspot.com/api")
                        .setRequestInterceptor(requestInterceptor)
                .build();
        HappyUserApi apiService =
                restAdapter.create(HappyUserApi.class);
        apiService.sendDetails(lat, lon, image, story,band,new Callback<HappyImage>() {
            @Override
            public void success(HappyImage user, Response response) {
                // Access user here after response is parsed
                Intent i=new Intent(ScanQRCode.this,TaskInterface.class);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

             //   Toast.makeText(getApplicationContext(), "sucess==" + response, Toast.LENGTH_LONG).show();
                Log.d("Response", response.toString());
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                // Log error here since request failed
                Toast.makeText(getApplicationContext(), "fail==" + retrofitError
                        , Toast.LENGTH_SHORT).show();
                Log.d("Response", retrofitError.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

         return;
    }

    @Override
    public void onLocationChanged(Location location) {
        Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
