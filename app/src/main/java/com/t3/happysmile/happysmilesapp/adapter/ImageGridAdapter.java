package com.t3.happysmile.happysmilesapp.adapter;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.model.ModelConnectionImage.Items;
import com.t3.happysmile.happysmilesapp.ui.activity.SingleImageView;

import java.util.ArrayList;

public class ImageGridAdapter extends BaseAdapter {

    ArrayList<Items> arrayImage;
    LayoutInflater inflater;
    Context context;
    Picasso pic;
    public ImageGridAdapter(Context applicationContext, ArrayList<Items> itemsimage)
    {
        this.context=applicationContext;
        this.arrayImage=itemsimage;
    }

    @Override
    public int getCount() {
        return arrayImage.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class ViewHolder
    {

        ImageView imageGrid;
    }

    @Override
    public View getView(final int position, View rowView, ViewGroup parent) {

        ViewHolder holder=new ViewHolder();

        inflater=(LayoutInflater)context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        rowView = inflater.inflate(R.layout.single_image_grid, parent,false);
        holder.imageGrid=(ImageView)rowView.findViewById(R.id.gridimage);
        Picasso.with(context).load(Uri.parse(arrayImage.get(position).getServingUrl())).into(holder.imageGrid);
        holder.imageGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SingleImageView.class);
                i.putExtra("imageurl", arrayImage.get(position).getServingUrl());
                i.putExtra("imagestory", arrayImage.get(position).getStory());
                context.startActivity(i);
            }
        });


        return rowView;
    }
}
