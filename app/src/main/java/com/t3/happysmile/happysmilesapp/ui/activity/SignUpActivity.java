package com.t3.happysmile.happysmilesapp.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;

import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.adapter.IconPageIndicator;
import com.t3.happysmile.happysmilesapp.adapter.PageIndicator;
import com.t3.happysmile.happysmilesapp.adapter.SignUpFragmentApdater;
import com.t3.happysmile.happysmilesapp.adapter.TestFragmentAdapter;


public class SignUpActivity extends FragmentActivity {
    SignUpFragmentApdater mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        /*getActionBar().hide();*/
        mAdapter = new SignUpFragmentApdater(getSupportFragmentManager(),getApplicationContext());

        mPager = (ViewPager)findViewById(R.id.pagersignup);
        mPager.setAdapter(mAdapter);

        mIndicator = (IconPageIndicator)findViewById(R.id.indicatorsignup);
        mIndicator.setViewPager(mPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_up, menu);
        return true;
    }


}
