package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.t3.happysmile.happysmilesapp.HappyUserApi;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.model.HappyImage;
import com.t3.happysmile.happysmilesapp.model.HappyUser;
import com.t3.happysmile.happysmilesapp.model.UploadImageData;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;


public class StoryActivity extends Activity {

    ImageView story_done;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    EditText edtstory;
    ScanQRCode data=new ScanQRCode();
    CameraActivity camera=new CameraActivity();
    String story;
    File image;
    String authKey;
    String username;
    String password;
    String band ;
    String lat;
    String lon;
    String credentials;
    static String imagePath;
    Bundle b;
    UploadImageData imge;
    String redirectUrl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);
        edtstory=(EditText)findViewById(R.id.edtStory);
      //  band=data.band;
         b=getIntent().getExtras();
         imge=new UploadImageData();
        /*getActionBar().hide();*/
        /* imge=b.get*/
        image=imge.getImage();//new File(imge.getMagePath());
       // image=;//camera.imageFile;
        pref=getApplication().getSharedPreferences("MyPref", 0);
        edit=pref.edit();
        story_done=(ImageView)findViewById(R.id.story_done);
        story_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                story = edtstory.getText().toString();
               /* Intent storydone=new Intent(StoryActivity.this,TaskInterface.class);
                startActivity(storydone);
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);*/
                edit.putInt("subTask", 3);
                edit.commit();
                sendData();
            }
        });
         imagePath=pref.getString("imagePath","1");
        Toast.makeText(StoryActivity.this, "imagePath="+imagePath, Toast.LENGTH_SHORT).show();
        imagePath=imagePath.substring(1);
        Log.d("imagePath==", imagePath);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_story, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
      if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void sendData()
    {
        //=pref.getString("authKey","");
         Toast.makeText(getApplicationContext(), "click", Toast.LENGTH_SHORT).show();
         username=pref.getString("username", "");
         password=pref.getString("password", "");
         band =pref.getString("bandcode","");
         lat=pref.getString("lat",""); //String.valueOf(data.latitude);
         lon=pref.getString("lon",""); //String.valueOf(data.longitude);

         credentials = username + ":" + password;
        final RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                authKey = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                request.addHeader("Authorization", authKey);
            }
        };
  // new RegUser().execute(lat,lon,edtstory.getText().toString());


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://happysmilesapi.appspot.com/api")
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        HappyUserApi apiService =
                restAdapter.create(HappyUserApi.class);
          apiService.sendDetails(lat, lon, image, band, edtstory.getText().toString(), new Callback<HappyImage>() {


              @Override
              public void success(HappyImage user, Response response) {
                  // Access user here after response is parsed

                  HappyUser userNew = user.getUser();
                  String userId = userNew.getUsername();
                  Intent i = new Intent(StoryActivity.this, Path.class);
                  startActivity(i);
                  overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//                Toast.makeText(getApplicationContext(), "sucess==" + response, Toast.LENGTH_LONG).show();
                  Log.d("Response===", response.getBody().toString());

              }

              @Override
              public void failure(RetrofitError retrofitError) {
                  // Log error here since request failed

                  List<Header> headers = retrofitError.getResponse().getHeaders();

                  for (Header header : headers) {
                      String KEY_HEADER_REDIRECT_LOCATION = "location";

                      if (KEY_HEADER_REDIRECT_LOCATION.equals(header.getName())) {
                           redirectUrl = header.getValue();

                          Log.d("redirectUrl", redirectUrl);
                          new redirectUrl().execute(redirectUrl);
                      }
                  }

              }
          });
    }
    class redirectUrl extends AsyncTask<String, Void, String> {
        String sss;
        @Override
        protected String doInBackground(String... params) {
            try {
                String url = params[0] + "lat=" + lat + "&lon=" + lon + "&bandid=" + band + "&story=" + edtstory.getText().toString();
                Log.d("urlRequest", url);
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost postRequest = new HttpPost(url);
                postRequest.addHeader("Authorization", authKey);
                MultipartEntity reqEntity = new MultipartEntity(
                        HttpMultipartMode.BROWSER_COMPATIBLE);
                // Log.d("image",image.toString());
                FileBody upload = new FileBody(new File(imagePath)
                );
                reqEntity.addPart("upload", upload);

                postRequest.setEntity(reqEntity);
                HttpResponse response = null;

                    response = httpClient.execute(postRequest);

                    sss = EntityUtils.toString(response.getEntity());
                    Log.d("sss==response", sss);
                } catch (Exception e) {
                    e.printStackTrace();
                Log.d("sss==response", sss + "Hello");
                 //   showAlert();
                }

                return sss;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(s==null)
            {
              ///  showAlert();
                Toast.makeText(StoryActivity.this,sss+"Data null",Toast.LENGTH_SHORT).show();
            }
            else
            {
                Intent i=new Intent(StoryActivity.this,Path.class);
                i.putExtra("currentPage",0);
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
            }
    }

    private void showAlert()
    {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(StoryActivity.this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Server Error");
        builder.setMessage("Could Not Upload Data now.... Server Error");
        builder.setPositiveButton("OK", null);

        builder.show();
    }
}
