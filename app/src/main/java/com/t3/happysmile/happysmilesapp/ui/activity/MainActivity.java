package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.WindowManager;

import com.t3.happysmile.happysmilesapp.R;


public class MainActivity extends Activity {

    private Thread mSplashThread;
    Boolean value;
    private SharedPreferences prefs;
    private String Userid,userId;
    SharedPreferences pref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        pref=getApplication().getSharedPreferences("MyPref", 0);
       value= pref.getBoolean("login", false);
        /*getActionBar().hide();*/
        /*SharedPreferences pref = getApplicationContext().getSharedPreferences(
                "UserID", MODE_PRIVATE);
        userId = pref.getString("userID", ""); // getting string
        prefs = PreferenceManager
                .getDefaultSharedPreferences(MainActivity.this);
        Userid = prefs.getString("flag", "0");
*/        try {

            mSplashThread = new Thread() {
                private Intent magicalintent;

                @Override
                public void run() {
                    try {
                        synchronized (this) {
                            wait(2000);

                        }
                    } catch (InterruptedException ex) {
                    } finally {
                          if(value){
                              magicalintent = new Intent(
                                      MainActivity.this,
                                      Path.class)

                                      .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                              magicalintent.putExtra("currentPage",1);

                              startActivity(magicalintent);
                              finish();
                          }else {

                              magicalintent = new Intent(
                                      MainActivity.this,
                                      LoginSignup.class)
                                      .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                              startActivity(magicalintent);
                              finish();
                          }

                       /* if (Userid.matches("1")) {
//						if (userId!=null) {
                           finish();

                        } else {

                            magicalintent = new Intent(
                                    MainActivity.this,
                                    LoginSignup.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                            ;
                            startActivity(magicalintent);
                            finish();

                        }*/
                    }

                }
            };

            mSplashThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
