package com.t3.happysmile.happysmilesapp;


import com.t3.happysmile.happysmilesapp.model.AccessToken;
import com.t3.happysmile.happysmilesapp.model.Connections;
import com.t3.happysmile.happysmilesapp.model.HappyImage;
import com.t3.happysmile.happysmilesapp.model.HappyUser;
import com.t3.happysmile.happysmilesapp.model.ModelConnectionImage.ConnectionsItem;
import com.t3.happysmile.happysmilesapp.model.User;

import java.io.File;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;

public interface HappyUserApi {

    @FormUrlEncoded
    @POST("/users")
    void sendDataRegistration(@Field("username") String username, @Field("email") String email, @Field("password") String password, Callback<User> cb);

    @FormUrlEncoded
    @POST("/oauth/token")
    void login(@Field("username") String username, @Field("password") String password, @Field("grant_type") String value, Callback<AccessToken> cb);

    @Multipart
    @POST("/images")
    void sendDetails(@Part("lat") String lat, @Part("lon") String lon, @Part("upload") File image, @Part("bandid") String bandid, @Part("story") String story, Callback<HappyImage> cb);


  @GET("/leaders/me")
    void getLeaders(Callback<Connections> cb);

    @GET("/images")
    void connectionImage(@Query("user") String user, Callback<ConnectionsItem> cb);

    @GET("/users")
    void search(@Query("q") String q, Callback<Connections> cb);


}
