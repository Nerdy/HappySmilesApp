package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.t3.happysmile.happysmilesapp.HappyUserApi;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.adapter.ImageGridAdapter;
import com.t3.happysmile.happysmilesapp.model.ModelConnectionImage.ConnectionsItem;
import com.t3.happysmile.happysmilesapp.model.ModelConnectionImage.Items;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ImageConnections extends Activity {

    String id;
    GridView gridView;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    ArrayList<Items> Itemsimage;
    String authKey;
    ImageGridAdapter imageAdapter;
    TextView textheaderimage;
    ImageView backarrow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_connections);
        Bundle b=getIntent().getExtras();
        id="5682617542246400";//b.getString("id");//
        String header=b.getString("username");
        pref=getApplication().getSharedPreferences("MyPref", 0);

        edit=pref.edit();
        /*getActionBar().hide();*/
        edit.putString("userIDforiamges",id);
        edit.putString("userNameforiamges", header);
        edit.commit();
        textheaderimage=(TextView)findViewById(R.id.textheaderimage);
        textheaderimage.setText(header);
        gridView=(GridView)findViewById(R.id.gridView);
        Itemsimage=new ArrayList<>();
        backarrow=(ImageView)findViewById(R.id.backarrow);
        String username=pref.getString("username", "");
        String password=pref.getString("password", "");
        final String credentials = username + ":" + password;
        backarrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                     Intent magicalintent = new Intent(
                        ImageConnections.this,Path.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                         magicalintent.putExtra("currentPage",2);
                        startActivity(magicalintent);
                        finish();
            }
        });
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                authKey = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                request.addHeader("Authorization", authKey);
            }
        };
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://happysmilesapi.appspot.com/api")
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        HappyUserApi apiService =
                restAdapter.create(HappyUserApi.class);

        apiService.connectionImage(id, new Callback<ConnectionsItem>() {
            @Override
            public void success(ConnectionsItem connectionsItem, Response response) {
                Items[] items=new Items[connectionsItem.getItems().length];
                items = connectionsItem.getItems();
                for (int i = 0; i < items.length; i++)
                {
                    Items itemsImage=new Items();
                    itemsImage.setId(items[i].getId());
                    itemsImage.setServingUrl(items[i].getServingUrl());
                    itemsImage.setStory(items[i].getStory());
                    Log.d("ConnectionImages", items[i].getStory());
                    Itemsimage.add(itemsImage);
                }
                imageAdapter=new ImageGridAdapter(ImageConnections.this, Itemsimage);
                gridView.setAdapter(imageAdapter);

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
