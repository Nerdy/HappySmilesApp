package com.t3.happysmile.happysmilesapp.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import com.t3.happysmile.happysmilesapp.FontCache;


public class CustomFontButton extends Button
{
    public CustomFontButton( Context context )
    {
        super( context );
        init( context, null, 0 );
    }

    public CustomFontButton( Context context, AttributeSet attrs )
    {
        super( context, attrs );
        init( context, attrs, 0 );
    }

    public CustomFontButton( Context context, AttributeSet attrs, int defStyleAttr )
    {
        super( context, attrs, defStyleAttr );
        init( context, attrs, defStyleAttr );
    }

    private void init( Context context, AttributeSet attrs, int defStyleAttr )
    {
        FontCache.applyToView(this, attrs);
    }
}
