package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.t3.happysmile.happysmilesapp.HappyUserApi;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.adapter.SearchConnectionAdapter;
import com.t3.happysmile.happysmilesapp.model.Connections;
import com.t3.happysmile.happysmilesapp.model.Items;

import java.util.ArrayList;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchConnections extends Activity {

    EditText searchedt;
    String authKey;
    TextWatcher tw;
    SearchConnectionAdapter adapter;
    ArrayList<Items> Itemscon;
    ListView connectionlist;
    String username;
    String password;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_connections);
        pref=getApplicationContext().getSharedPreferences("MyPref", 0);
        edit=pref.edit();
        connectionlist=(ListView)findViewById(R.id.listView);
        searchedt=(EditText)findViewById(R.id.searchedt);
        Itemscon=new ArrayList<>();
        /*getActionBar().hide();*/
        username=pref.getString("username", "");
         password=pref.getString("password", "");
        tw=new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                try {
                    filter(s.toString());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }};
        searchedt.addTextChangedListener(tw);

    }
    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        Itemscon.clear();
        if (charText.length() >= 3) {
            searchConnection(charText);
        }
        else
        {

        }

    }

     void searchConnection(String charText)
     {
         final String credentials = username + ":" + password;
         RequestInterceptor requestInterceptor = new RequestInterceptor() {
             @Override
             public void intercept(RequestFacade request) {
                 authKey = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                 request.addHeader("Authorization", authKey);
             }
         };
         RestAdapter restAdapter = new RestAdapter.Builder()
                 .setEndpoint("https://happysmilesapi.appspot.com/api")
                 .setRequestInterceptor(requestInterceptor)
                 .setLogLevel(RestAdapter.LogLevel.FULL)
                 .build();
         HappyUserApi apiService =
                 restAdapter.create(HappyUserApi.class);
         apiService.search(charText, new Callback<Connections>() {


             @Override
             public void success(Connections connections, Response response) {


                 Items[] items = new Items[connections.getItems().length];
                 items = connections.getItems();
                 for (int i = 0; i < items.length; i++) {
                     Items listItems = new Items();
                     listItems.setId(items[i].getId());
                     listItems.setUsername(items[i].getUsername());
                     Itemscon.add(listItems);
                 }
                 for(int j=0; j<Itemscon.size();j++)
                 {
                     Log.d("GetdataCOnnection", Itemscon.get(j).getUsername());
                 }
                 adapter = new SearchConnectionAdapter(SearchConnections.this, Itemscon);
                 connectionlist.setAdapter(adapter);
             }

             @Override
             public void failure(RetrofitError retrofitError) {
                 // Log error here since request failed
                 Toast.makeText(getApplicationContext(), "fail==connection" + retrofitError
                         , Toast.LENGTH_SHORT).show();
                 Log.d("Response", retrofitError.toString());
             }
         });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search_connections, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
