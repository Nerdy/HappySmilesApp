package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;


import com.t3.happysmile.happysmilesapp.Dailog;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.ui.fragment.ConnectionFragment;
import com.t3.happysmile.happysmilesapp.ui.fragment.PathFragment;
import com.t3.happysmile.happysmilesapp.ui.fragment.ScanQRCodeFragmet;

import java.util.ArrayList;
import java.util.List;


public class Path extends FragmentActivity {
    private SimpleGestureFilter detector;
     /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    MyPageAdapter pageAdapter;
    String ScanBand;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    int currentPage;
    Dailog d;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path);
        d=new Dailog();
        pref=getApplication().getSharedPreferences("MyPref", 0);
        edit=pref.edit();
        ScanBand=pref.getString("ScanBand","No");
        // Detect touched area
       // detector = new SimpleGestureFilter(Path.this,this);
        /*getActionBar().hide();*/
        Bundle b=getIntent().getExtras();
        currentPage=b.getInt("currentPage",1);
        List<Fragment> fragments = getFragments();

        pageAdapter = new MyPageAdapter(getSupportFragmentManager(), fragments);

        ViewPager pager = (ViewPager)findViewById(R.id.pager);

        pager.setAdapter(pageAdapter);
        pager.setCurrentItem(currentPage);

    }


    class MyPageAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;

        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {

            super(fm);

            this.fragments = fragments;

        }

        @Override
        public Fragment getItem(int position) {

            return this.fragments.get(position);

        }

        @Override
        public int getCount() {

            return this.fragments.size();

        }

    }
    private List<Fragment> getFragments(){

        List<Fragment> fList = new ArrayList<Fragment>();
if(ScanBand.equals("yes")) {
    fList.add(ScanQRCodeFragmet.newInstance("Fragment 4"));
}else{
    fList.add(TaskInterface.newInstance("Fragment 1"));
}
        fList.add(PathFragment.newInstance("Fragment 2"));

        fList.add(ConnectionFragment.newInstance("Fragment 3"));

        return fList;

    }

    /*@Override
    public boolean dispatchTouchEvent(MotionEvent me){
        // Call onTouchEvent of SimpleGestureFilter class
        this.detector.onTouchEvent(me);
        return super.dispatchTouchEvent(me);
    }*/
   /* @Override
    public void onSwipe(int direction) {
        String str = "";

        switch (direction) {

            case SimpleGestureFilter.SWIPE_RIGHT :
                str = "Swipe Right";
                Intent right=new Intent(Path.this,TaskDescription.class);
                startActivity(right);
                break;
            case SimpleGestureFilter.SWIPE_LEFT :
                str = "Swipe Left";
                Intent left=new Intent(Path.this,TaskInterface.class);
                startActivity(left);
                break;
            case SimpleGestureFilter.SWIPE_DOWN :
                str = "Swipe Down";
                break;
            case SimpleGestureFilter.SWIPE_UP :
                str = "Swipe Up";
                break;

        }
        Toast.makeText(this, str, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDoubleTap() {
        Toast.makeText(this, "Double Tap", Toast.LENGTH_SHORT).show();
    }
*/

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Alert")
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Path.this.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
      //  d.showAlertDialogExit(Path.this, "Alert", "Do you want to exit?");

    }
}