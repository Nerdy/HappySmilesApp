package com.t3.happysmile.happysmilesapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

@SuppressWarnings( "UnusedDeclaration" )
public class HappyUser implements Parcelable
{
    private String id;

    private String username;

    private String description;

    private Date regTime;

    private boolean follows;

    private boolean leads;

    private int followerCount;

    private int leaderCount;

    private HappyUser() { }

    public String getId()
    {
        return id;
    }

    public String getUsername()
    {
        return username;
    }

    public String getDescription()
    {
        return description;
    }

    public Date getRegTime()
    {
        return regTime;
    }

    public boolean isFollows()
    {
        return follows;
    }

    public boolean isLeads()
    {
        return leads;
    }

    public void setLeads( boolean leads )
    {
        this.leads = leads;
    }

    public int getFollowerCount()
    {
        return followerCount;
    }

    public void incFollowerCount()
    {
        //should not overflow
        if( followerCount < Integer.MAX_VALUE ) this.followerCount++;
    }

    public void decFollowerCount()
    {
        //should never fall below zero
        if( followerCount > 0 ) this.followerCount--;
    }

    public int getLeaderCount()
    {
        return leaderCount;
    }

    public void incLeaderCount()
    {
        //should not overflow
        if( leaderCount < Integer.MAX_VALUE ) this.leaderCount++;
    }

    public void decLeaderCount()
    {
        //should never fall below zero
        if( leaderCount > 0 ) this.leaderCount--;
    }

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeString( this.id );
        dest.writeString( this.username );
        dest.writeString( this.description );
        dest.writeLong( regTime != null ? regTime.getTime() : -1 );
        dest.writeByte( follows ? ( byte ) 1 : ( byte ) 0 );
        dest.writeByte( leads ? ( byte ) 1 : ( byte ) 0 );
        dest.writeInt( this.followerCount );
        dest.writeInt( this.leaderCount );
    }

    public void readFromParcel( Parcel in )
    {
        this.id = in.readString();
        this.username = in.readString();
        this.description = in.readString();
        long tmpRegTime = in.readLong();
        this.regTime = tmpRegTime == -1 ? null : new Date( tmpRegTime );
        this.follows = in.readByte() != 0;
        this.leads = in.readByte() != 0;
        this.followerCount = in.readInt();
        this.leaderCount = in.readInt();
    }

    private HappyUser( Parcel in )
    {
        readFromParcel( in );
    }

    public static final Creator<HappyUser> CREATOR = new Creator<HappyUser>()
    {
        public HappyUser createFromParcel( Parcel source ) {return new HappyUser( source );}

        public HappyUser[] newArray( int size ) {return new HappyUser[size];}
    };

    @Override
    public boolean equals( Object o )
    {
        if( this == o ) return true;
        if( !( o instanceof HappyUser ) ) return false;

        HappyUser happyUser = ( HappyUser ) o;

        return id.equals( happyUser.id );

    }

    @Override
    public int hashCode()
    {
        return id.hashCode();
    }
}