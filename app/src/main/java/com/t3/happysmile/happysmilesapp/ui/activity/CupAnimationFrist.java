package com.t3.happysmile.happysmilesapp.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.t3.happysmile.happysmilesapp.R;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class CupAnimationFrist extends Activity {
    private ImageView _imagView;
    private Timer _timer,Newtimer;
    private static int _index,newIndex;
    private MyHandler handler;
    private ImageView view;
    private AnimationDrawable frameAnimation;
    RelativeLayout btnRelative;
    ImageView let_go_animation;
    private NewHandler newHandler;
    MediaPlayer mp1,mp2 ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cup_animation_frist);
        handler= new MyHandler();
        newHandler=new NewHandler();
        mp1=MediaPlayer.create(this, R.raw.trophy);
        mp2=MediaPlayer.create(this, R.raw.trophyplaced);
        _imagView=(ImageView) findViewById(R.id.imageView);
        btnRelative=(RelativeLayout)findViewById(R.id.btnRelative);
        let_go_animation=(ImageView)findViewById(R.id.let_go_animation);
        newIndex=268;
        _index = 172;
          Newtimer=new Timer();
        _timer = new Timer();
        _timer.schedule(new TickClass(), 100, 75);
       // Newtimer.schedule(new NewTickClass(), 150, 65);
     //   btnRelative.setVisibility(View.VISIBLE);
        let_go_animation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent login = new Intent(CupAnimationFrist.this, VideoActivity.class);
                startActivity(login);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });
    }

    private class TickClass extends TimerTask
    {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            handler.sendEmptyMessage(_index);

            if(_index < 282)
            {
                _index++;
            }/*else
            {
                _index=268;
               // btnRelative.setVisibility(View.VISIBLE);
                _timer.schedule(new TickClass(), 120, 55);
               // Newtimer.schedule(new NewTickClass(), 120, 55);
               //
            }*/
        }
    }
    private class NewTickClass extends TimerTask
    {
        @Override
        public void run() {
            // TODO Auto-generated method stub
            newHandler.sendEmptyMessage(newIndex);

            if(newIndex<282) {
                newIndex++;
            }/*else
            {

                newIndex=268;
                Newtimer.schedule(new NewTickClass(), 100, 75);

            }*/

               /* for(newIndex =268;newIndex<=282;newIndex++)
            {
                if(newIndex==282)
                    newIndex=268;

            }*/
            /*if(_index < 282)
            {
                _index++;
            }else
            {
               // Newtimer.schedule(new TickClass(), 150, 65);
                // btnRelative.setVisibility(View.VISIBLE);
            }*/
        }
    }
    private class MyHandler extends Handler
    {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);

            try {
                Bitmap bmp= BitmapFactory.decodeStream(CupAnimationFrist.this.getAssets().open("animation_00" + _index + ".jpg"));

               // ImageView img = new ImageView(CupAnimationFrist.this);
                _imagView.setImageBitmap(bmp);

               /* RelativeLayout.LayoutParams imgParams = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                img.setLayoutParams(imgParams);
                _imagView.addView(img);
*/
                if(_index==176)
                    mp1.start();
                if(_index==231) {
                    mp1.stop();
                    mp2.start();
                }
                    if(_index==282) {
                        btnRelative.setVisibility(View.VISIBLE);
                       // Newtimer.schedule(new NewTickClass(), 75, 75);
                    }

            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        }
    }
    private class NewHandler extends Handler
    {
        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);

            try {
                Bitmap bmp= BitmapFactory.decodeStream(CupAnimationFrist.this.getAssets().open("animation_00" + newIndex + ".jpg"));
                ImageView img = new ImageView(CupAnimationFrist.this);
                _imagView.setImageBitmap(bmp);

                /*RelativeLayout.LayoutParams imgParams = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
                img.setLayoutParams(imgParams);
                _imagView.addView(img);
*/
            } catch (IOException e) {
                // TODO Auto-generated catch block
            }
        }
    }


}
