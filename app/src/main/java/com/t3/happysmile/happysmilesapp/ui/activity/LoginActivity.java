package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.itwenty.passwordedittext.PasswordEditText;
import com.t3.happysmile.happysmilesapp.ConnectionDetector;
import com.t3.happysmile.happysmilesapp.Dailog;
import com.t3.happysmile.happysmilesapp.HappyUserApi;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.model.AccessToken;
import com.t3.happysmile.happysmilesapp.views.ShowAlert;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LoginActivity extends Activity {

    Button loginBtn;
    EditText edtusername;
    PasswordEditText edtpassword;
    TextView signup_here,forgetPassword;
    static String username=null,password=null;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    LinearLayout login_container;
    String string; //authorization key
    ShowAlert objAlert;
    View view;
    Dailog d;
    ConnectionDetector cd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        pref=getApplication().getSharedPreferences("MyPref", 0);
        edit=pref.edit();
        objAlert=new ShowAlert();

        /*getActionBar().hide();*/

        edtusername=(EditText)findViewById(R.id.usernameLogin);
        signup_here=(TextView)findViewById(R.id.signup_here);
        edtpassword=(PasswordEditText)findViewById(R.id.passwordLogin);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        login_container=(LinearLayout)findViewById(R.id.login_container);
         edtusername.setTypeface(null, Typeface.BOLD);
        loginBtn=(Button)findViewById(R.id.loginBtn);
        forgetPassword=(TextView)findViewById(R.id.forgetPassword);
//        username=edtusername.getText().toString();
//        password=edtpassword.getText().toString();
        d=new Dailog();
        cd=new ConnectionDetector(this);
        forgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(LoginActivity.this,EnterEmailId.class);
                startActivity(i);
            }
        });
        signup_here.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginbtn = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(loginbtn);
                overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_left);
            }
        });
         view = this.getCurrentFocus();
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Login Data======", edtusername.getText().toString() + "==Value==" + edtpassword.getText().toString());
                String data = pref.getString("userId", "");
                String user = pref.getString("username", "");

                 if (cd.isConnectingToInternet() == "internet") {
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
//                Toast.makeText(getApplication(),user+"user data"+data,Toast.LENGTH_LONG).show();
                    Log.d("userdata", user + "user data" + data);
                    if (!(edtusername.getText().toString()).isEmpty() && !(edtpassword.getText().toString()).isEmpty()) {
                        loginBtn.setText("Logging in...");
                        //   Toast.makeText(getApplicationContext(),username+" data "+password, Toast.LENGTH_SHORT).show();
                        final String credentials = edtusername.getText().toString() + ":" + edtpassword.getText().toString();

                        RequestInterceptor requestInterceptor = new RequestInterceptor() {
                            @Override
                            public void intercept(RequestFacade request) {
                                string = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                                request.addHeader("Authorization", string);
                            }
                        };


                        RestAdapter restAdapter = new RestAdapter.Builder()
                                .setEndpoint("https://happysmilesapi.appspot.com/api")
                                        /// .setRequestInterceptor(requestInterceptor)
                                .setLogLevel(RestAdapter.LogLevel.FULL)
                                .build();
                        HappyUserApi apiService =
                                restAdapter.create(HappyUserApi.class);
                        String grant_type = "password";
                        apiService.login(edtusername.getText().toString(), edtpassword.getText().toString(), grant_type, new Callback<AccessToken>() {
                            @Override
                            public void success(AccessToken user, Response response) {
                                // Access user here after response is parsed
                                edit.putString("username", edtusername.getText().toString());
                                edit.putString("password", edtpassword.getText().toString());
                                edit.putString("authKey", string);
                                //  edit.putBoolean("login",true);
                                edit.commit();


                                Intent magicalintent = new Intent(
                                        LoginActivity.this,
                                        Path.class);
                                magicalintent.putExtra("currentPage", 1);
                                startActivity(magicalintent);
                                finish();
                                /*Intent loginbtn = new Intent(LoginActivity.this, ScanQRCode.class);
                                startActivity(loginbtn);
                                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                Log.d("Response", response.toString());*/
                            }

                            @Override
                            public void failure(RetrofitError retrofitError) {
                                // Log error here since request failed
                           /* Toast.makeText(getApplicationContext(), "fail==" + retrofitError
                                    , Toast.LENGTH_SHORT).show();*/
                                Log.d("Response", retrofitError.toString()+"Data"+retrofitError.getResponse().getStatus());

                                if (retrofitError.getResponse().getStatus() == 400) {
                                   /* String msg = "PLease Enter Correct Login Details";
                                    objAlert.showLoginAlert(LoginActivity.this, msg);
                                    Toast.makeText(LoginActivity.this, "Invalid username or passwor", Toast.LENGTH_SHORT).show();*/
                                    d.showAlertDialog(LoginActivity.this, "InCorrect Data", "PLease Enter Correct Login Details");
                                    loginBtn.setText("ENTER");
                                }

                            }
                        });
                    } else {
                        Toast.makeText(getApplicationContext(), "Please Enter Login Details", Toast.LENGTH_LONG).show();
                    }

                }else{

                    d.showAlertDialog(LoginActivity.this, "No Internet Connection", "Please Connect To Internet");
                }
            }


        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
