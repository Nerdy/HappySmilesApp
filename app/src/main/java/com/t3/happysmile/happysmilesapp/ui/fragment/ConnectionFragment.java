package com.t3.happysmile.happysmilesapp.ui.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.t3.happysmile.happysmilesapp.HappyUserApi;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.adapter.ConnectionAdapter;
import com.t3.happysmile.happysmilesapp.model.Connections;
import com.t3.happysmile.happysmilesapp.model.Items;
import com.t3.happysmile.happysmilesapp.ui.activity.MapsActivity;
import com.t3.happysmile.happysmilesapp.ui.activity.MyProfile;
import com.t3.happysmile.happysmilesapp.ui.activity.SearchConnections;
import com.t3.happysmile.happysmilesapp.ui.activity.Settings;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the

 * Use the {@link ConnectionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConnectionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    ImageView menuList;
    TextView trackBand,myprofile,setting,aboutus,happystore,upgrade;
    EditText edtsearch;
    ImageView sreachImage;
    TextView text;
    View view_ll;
    LinearLayout more_menu_layout;
    boolean more_menu=false;
    static int i=1;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    ArrayList<Items> Itemscon;
    ListView connectionlist;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String authKey;
    TextWatcher tw;
    ConnectionAdapter adapter;
    // TODO: Rename and change types and number of parameters
    public static ConnectionFragment newInstance(String param1) {
        ConnectionFragment fragment = new ConnectionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public ConnectionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_connection, container, false);
        view_ll=(View)view.findViewById(R.id.view_ll_con);
        pref=getActivity().getSharedPreferences("MyPref", 0);
        edit=pref.edit();

        connectionlist=(ListView)view.findViewById(R.id.connectionlist);
        more_menu_layout=(LinearLayout)view.findViewById(R.id.more_menu_layout_con);
        edtsearch=(EditText)view.findViewById(R.id.edtsearch);
        sreachImage=(ImageView)view.findViewById(R.id.search_image);
        text=(TextView)view.findViewById(R.id.textheader);
        menuList=(ImageView)view.findViewById(R.id.menuList_con);
        trackBand =(TextView)view.findViewById(R.id.trackBand_con);
        myprofile=(TextView)view.findViewById(R.id.myProfile_con);
        setting =(TextView)view.findViewById(R.id.settings_con);
        aboutus=(TextView)view.findViewById(R.id.aboutUs_con);
        happystore=(TextView)view.findViewById(R.id.happUser_con);
        upgrade=(TextView)view.findViewById(R.id.upgrade_con);
        String username=pref.getString("username", "");
        String password=pref.getString("password", "");
        Itemscon=new ArrayList<Items>();


        final String credentials = username + ":" + password;
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                authKey = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                request.addHeader("Authorization", authKey);
            }
        };
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://happysmilesapi.appspot.com/api")
                .setRequestInterceptor(requestInterceptor)
                .build();
        HappyUserApi apiService =
                restAdapter.create(HappyUserApi.class);
        apiService.getLeaders(new Callback<Connections>() {


            @Override
            public void success(Connections connections, Response response) {


                Items[] items = new Items[connections.getItems().length];
                items = connections.getItems();
                for (int i = 0; i < items.length; i++) {
                    Items listItems = new Items();
                    listItems.setId(items[i].getId());
                    listItems.setUsername(items[i].getUsername());
                    Log.d("Connectiondata   " + i, items[i].getUsername());

                    Itemscon.add(listItems);
                }

                for(int j=0; j<Itemscon.size();j++)
                {
                   Log.d("GetdataCOnnection", Itemscon.get(j).getUsername());
                }
                adapter=new ConnectionAdapter(getActivity(), Itemscon);
                connectionlist.setAdapter(adapter);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                // Log error here since request failed
                Toast.makeText(getActivity(), "fail==connection" + retrofitError
                        , Toast.LENGTH_SHORT).show();
                Log.d("Response", retrofitError.toString());
            }
        });

        sreachImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Intent i=new Intent(getActivity(), SearchConnections.class);
                startActivity(i);


               /* if(i==1)
                {
                    edtsearch.setVisibility(View.VISIBLE);
                    text.setVisibility(View.INVISIBLE);
                    i=0;
                }else
                {
                    text.setVisibility(View.VISIBLE);
                    edtsearch.setVisibility(View.INVISIBLE);
                    i=1;
                }*/

            }
        });
        tw=new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                try {
                    adapter.filter(s.toString());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }};
        edtsearch.addTextChangedListener(tw);

        view_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (more_menu == false) {

                } else {

                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu = false;
                    view_ll.setVisibility(View.INVISIBLE);

                }
            }
        });
        trackBand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectTaskIntent=new Intent(getActivity(),MapsActivity.class);
                selectTaskIntent.putExtra("class", "fragment");
                selectTaskIntent.putExtra("className","TaskInterface");
                selectTaskIntent.putExtra("page",2);
                startActivity(selectTaskIntent);
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
            }
        });
        myprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectTaskIntent=new Intent(getActivity(),MyProfile.class);
                selectTaskIntent.putExtra("class", "fragment");
                selectTaskIntent.putExtra("className","TaskInterface");
                selectTaskIntent.putExtra("page",2);
                startActivity(selectTaskIntent);
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);

            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectTaskIntent=new Intent(getActivity(),Settings.class);
                selectTaskIntent.putExtra("class", "fragment");
                selectTaskIntent.putExtra("className","TaskInterface");
                selectTaskIntent.putExtra("page",2);
                startActivity(selectTaskIntent);
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
            }
        });
        aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
            }
        });
        happystore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
            }
        });

        upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                more_menu_layout.setVisibility(View.INVISIBLE);
                more_menu=false;
                view_ll.setVisibility(View.INVISIBLE);
            }
        });

        menuList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (more_menu == false) {
                    more_menu_layout.setVisibility(View.VISIBLE);
                    view_ll.setVisibility(View.VISIBLE);
                    more_menu = true;
                } else {
                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu = false;
                    view_ll.setVisibility(View.INVISIBLE);
                }
            }
        });
    return view;
    }
}
