package com.t3.happysmile.happysmilesapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class GeoPt implements Parcelable
{
    private float latitude;
    private float longitude;

    public GeoPt( float latitude, float longitude )
    {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private GeoPt() { }

    public float getLatitude()
    {
        return this.latitude;
    }

    public float getLongitude()
    {
        return this.longitude;
    }

    public String toString()
    {
        return String.format("%f,%f", this.latitude, this.longitude);
    }

    @Override
    public int describeContents() { return 0; }

    @Override
    public void writeToParcel( Parcel dest, int flags )
    {
        dest.writeFloat( this.latitude );
        dest.writeFloat( this.longitude );
    }

    protected GeoPt( Parcel in )
    {
        this.latitude = in.readFloat();
        this.longitude = in.readFloat();
    }

    public static final Creator<GeoPt> CREATOR = new Creator<GeoPt>()
    {
        public GeoPt createFromParcel( Parcel source ) {return new GeoPt( source );}

        public GeoPt[] newArray( int size ) {return new GeoPt[size];}
    };

    @Override
    public boolean equals( Object o )
    {
        if( this == o ) return true;
        if( !( o instanceof GeoPt ) ) return false;

        GeoPt geoPt = ( GeoPt ) o;

        if( Float.compare(geoPt.latitude, latitude) != 0 ) return false;
        return Float.compare(geoPt.longitude, longitude) == 0;
    }

    @Override
    public int hashCode()
    {
        int result = ( latitude != +0.0f ? Float.floatToIntBits(latitude) : 0 );
        result = 31 * result + ( longitude != +0.0f ? Float.floatToIntBits(longitude) : 0 );
        return result;
    }
}