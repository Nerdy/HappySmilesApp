package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.t3.happysmile.happysmilesapp.ConnectionDetector;
import com.t3.happysmile.happysmilesapp.Dailog;
import com.t3.happysmile.happysmilesapp.R;

public class LoginSignup extends Activity {
    View login, signup;
    //  //dGVzdDp0ZXN0MTIzNA==
    ConnectionDetector cd;
     MediaPlayer mp ;
    Dailog d;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);
        cd=new ConnectionDetector(this);
        d=new Dailog();
         mp = MediaPlayer.create(this, R.raw.btnclick1);
        login=(View)findViewById(R.id.login);
        signup=(View)findViewById(R.id.signup);

        /*getActionBar().hide();*/
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                if(cd.isConnectingToInternet()=="internet") {
                    Intent login = new Intent(LoginSignup.this, LoginActivity.class);
                    startActivity(login);

                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }else{

                   d.showAlertDialog(LoginSignup.this,"No Internet Connection","Please Connect To Internet");
                }
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mp.start();
                Intent login=new Intent(LoginSignup.this,SignUpActivity.class);
                startActivity(login);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login_signup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
