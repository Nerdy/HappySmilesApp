package com.t3.happysmile.happysmilesapp;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.Hashtable;

public class FontCache
{

    //Ensure the font files specified here actually exist
    //in assets/ folder or expect a nasty RuntimeException
    public enum Font
    {
        LuckiestGuy( "luckiest_guy.ttf" ),
        MuseoSans( "museo_sans.ttf" );

        public final String mFontName;

        Font( String fontName )
        {
            mFontName = fontName;
        }

        public static Font forId( int id )
        {
            switch( id )
            {
                case 0:
                    return LuckiestGuy;
                case 1:
                    return MuseoSans;
            }
            return null;
        }
    }

    private static final Hashtable<String, Typeface> fontCache = new Hashtable<>();

    public static Typeface get( int id, Context context )
    {
        Font font = Font.forId(id);
        if( null == font )
            return null;

        String name = font.mFontName;
        Typeface tf = fontCache.get( font.mFontName );
        if( tf == null )
        {
            // Will throw RT if font with given name is not found
            tf = Typeface.createFromAsset(context.getAssets(), name);
            fontCache.put( name, tf );
        }
        return tf;
    }

    public static void applyToView( TextView tv, @Nullable AttributeSet attrs )
    {
        if( null == attrs )
            return;

        int fontId = -1;
        TypedArray a = tv.getContext().obtainStyledAttributes( attrs, R.styleable.CustomFontView );
        if( null != a )
        {
            fontId = a.getInt( R.styleable.CustomFontView_font, -1 );
            a.recycle();
        }
        if( fontId != -1 )
        {
            Typeface tf = FontCache.get( fontId, tv.getContext() );
            if( null != tf ) tv.setTypeface( tf );
        }
    }
}