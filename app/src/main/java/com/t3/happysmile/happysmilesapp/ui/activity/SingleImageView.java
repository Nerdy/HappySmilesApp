package com.t3.happysmile.happysmilesapp.ui.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.t3.happysmile.happysmilesapp.R;

public class SingleImageView extends Activity {
   String story,imageUrl;
    ImageView showstory,backfromicon,backfromstroy,backgroundImage,backtopicfrompic;
    FrameLayout overlay;
    FrameLayout storyoverlay;
    TextView storyTv;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    String userIDforiamges,userNameforiamges;
    boolean overlaybac=false;
    RelativeLayout rl_imageSet;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_image_view);
        context=getApplicationContext();
        pref=getApplication().getSharedPreferences("MyPref", 0);
        Bundle b=getIntent().getExtras();
        story=b.getString("imagestory");
        imageUrl=b.getString("imageurl");
        /*getActionBar().hide();*/
        try {
            initialize();
        } catch (Exception e) {
            e.printStackTrace();
          //  BaseException.run();
        }

        //Toast.makeText(SingleImageView.this,story+"Data"+imageUrl,Toast.LENGTH_LONG).show();
        Picasso.with(this).load(Uri.parse(imageUrl)).into(new Target(){

            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                rl_imageSet.setBackground(new BitmapDrawable(context.getResources(), bitmap));
            }

            @Override
            public void onBitmapFailed(final Drawable errorDrawable) {
                Log.d("TAG", "FAILED");
            }

            @Override
            public void onPrepareLoad(final Drawable placeHolderDrawable) {
                Log.d("TAG", "Prepare Load");
            }
        });
        storyTv.setText(story);
        backgroundImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (overlaybac == false) {
                    overlay.setVisibility(View.VISIBLE);
                    overlaybac = true;
                } else {

                }
            }
        });
        showstory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                storyoverlay.setVisibility(View.VISIBLE);
                overlay.setVisibility(View.INVISIBLE);
            }
        });
        backfromicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SingleImageView.this,ImageConnections.class);
                i.putExtra("id",userIDforiamges);
                i.putExtra("username", userNameforiamges);
                startActivity(i);
            }
        });
        backtopicfrompic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SingleImageView.this,ImageConnections.class);
                i.putExtra("id",userIDforiamges);
                i.putExtra("username", userNameforiamges);
                startActivity(i);
            }
        });
        backfromstroy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SingleImageView.this,ImageConnections.class);
                i.putExtra("id",userIDforiamges);
                i.putExtra("username",userNameforiamges);
                startActivity(i);
            }
        });
    }

    private void initialize(){

        userIDforiamges = pref.getString("userIDforiamges","");
        userNameforiamges = pref.getString("userNameforiamges","");
        rl_imageSet=(RelativeLayout)findViewById(R.id.rl_imageSet);
        storyTv=(TextView)findViewById(R.id.storyDetail);
        overlay=(FrameLayout)findViewById(R.id.overlay);
        storyoverlay=(FrameLayout)findViewById(R.id.storyOverlay);
        backgroundImage=(ImageView)findViewById(R.id.singleImage);
        showstory=(ImageView)findViewById(R.id.showstory);
        backfromicon=(ImageView)findViewById(R.id.backfromicon);
        backfromstroy=(ImageView)findViewById(R.id.backfromstory);
        backtopicfrompic=(ImageView)findViewById(R.id.backtopicfrompic);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_single_image_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
