package com.t3.happysmile.happysmilesapp.views;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class BetterViewPager extends ViewPager
{
    private boolean mPagingEnabled = true;

    public BetterViewPager( Context context )
    {
        super( context );
    }

    public BetterViewPager( Context context, AttributeSet attrs )
    {
        super( context, attrs );
    }

    @Override
    public boolean onInterceptTouchEvent( MotionEvent ev )
    {
        return mPagingEnabled && super.onInterceptTouchEvent( ev );
    }

    @Override
    public boolean onTouchEvent( MotionEvent ev )
    {
        return mPagingEnabled && super.onTouchEvent( ev );
    }

    public void setPagingEnabled( boolean pagingEnabled )
    {
        mPagingEnabled = pagingEnabled;
    }
}
