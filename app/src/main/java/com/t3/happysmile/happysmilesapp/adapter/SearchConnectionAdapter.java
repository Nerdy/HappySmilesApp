package com.t3.happysmile.happysmilesapp.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.model.Items;
import com.t3.happysmile.happysmilesapp.ui.activity.SearchConnections;

import java.util.ArrayList;
import java.util.List;

public class SearchConnectionAdapter extends BaseAdapter {
    List<Items> Itemscon;
    ArrayList<Items> arraylist;
    LayoutInflater inflater;
    Context context;
    public SearchConnectionAdapter(SearchConnections searchConnections, ArrayList<Items> itemscon) {
        this.context=searchConnections;
        this.Itemscon=itemscon;
        this.arraylist = new ArrayList<Items>();
        this.arraylist.addAll(itemscon);
        inflater = (LayoutInflater)context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return Itemscon.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class ViewHolder
    {
        TextView usernameTv;
        ImageView ping,profile;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder=new ViewHolder();
        convertView = inflater.inflate(R.layout.connection_list_item, parent,false);
        holder.usernameTv=(TextView) convertView.findViewById(R.id.connectionUsername);
        holder.ping=(ImageView) convertView.findViewById(R.id.ping_item);
        holder.profile=(ImageView) convertView.findViewById(R.id.Iv_profle_connection);
        holder.usernameTv.setText(Itemscon.get(position).getUsername());
        holder.ping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Ping", Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }


}
