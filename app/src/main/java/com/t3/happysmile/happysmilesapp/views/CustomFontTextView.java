package com.t3.happysmile.happysmilesapp.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.t3.happysmile.happysmilesapp.FontCache;


public class CustomFontTextView extends TextView
{
    public CustomFontTextView( Context context )
    {
        super( context );
        init( context, null, 0 );
    }

    public CustomFontTextView( Context context, AttributeSet attrs )
    {
        super( context, attrs );
        init( context, attrs, 0 );
    }

    public CustomFontTextView( Context context, AttributeSet attrs, int defStyleAttr )
    {
        super( context, attrs, defStyleAttr );
        init( context, attrs, defStyleAttr );
    }

    private void init( Context context, AttributeSet attrs, int defStyleAttr )
    {
        FontCache.applyToView(this, attrs);
    }
}
