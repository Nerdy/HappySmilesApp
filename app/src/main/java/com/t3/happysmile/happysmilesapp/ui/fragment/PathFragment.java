package com.t3.happysmile.happysmilesapp.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.t3.happysmile.happysmilesapp.Dailog;
import com.t3.happysmile.happysmilesapp.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link PathFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PathFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    ImageView menuList;
    TextView trackBand,myprofile,setting,aboutus,happystore,upgrade;
    View view_ll;
    LinearLayout more_menu_layout;
    Dailog d;
    boolean more_menu=false;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.

     * @return A new instance of fragment PathFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PathFragment newInstance(String param1) {
        PathFragment fragment = new PathFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);

        fragment.setArguments(args);
        return fragment;
    }

    public PathFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_path, container, false);
        d=new Dailog();

       /* view_ll=(View)view.findViewById(R.id.view_ll_path);
        more_menu_layout=(LinearLayout)view.findViewById(R.id.more_menu_layout_path);

        menuList=(ImageView)view.findViewById(R.id.menuList_path);
        trackBand =(TextView)view.findViewById(R.id.trackBand_path);
        myprofile=(TextView)view.findViewById(R.id.myProfile_path);
        setting =(TextView)view.findViewById(R.id.settings_path);
        aboutus=(TextView)view.findViewById(R.id.aboutUs_path);
        happystore=(TextView)view.findViewById(R.id.happUser_path);
        upgrade=(TextView)view.findViewById(R.id.upgrade_path);
*/

       /* view_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(more_menu==false)
                {

                }else
                {

                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu=false;
                    view_ll.setVisibility(View.INVISIBLE);

                }
            }
        });
        trackBand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent selectTaskIntent=new Intent(getActivity(),MapsActivity.class);
                startActivity(selectTaskIntent);
            }
        });
        myprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               *//* Intent selectTaskIntent=new Intent(getActivity(),MyProfile.class);
                startActivity(selectTaskIntent);*//*

            }
        });
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        aboutus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        happystore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        upgrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        menuList.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (more_menu == false) {
                    more_menu_layout.setVisibility(View.VISIBLE);
                    view_ll.setVisibility(View.VISIBLE);
                    more_menu = true;
                } else {
                    more_menu_layout.setVisibility(View.INVISIBLE);
                    more_menu = false;
                    view_ll.setVisibility(View.INVISIBLE);
                }
            }
        });*/
        return view ;
    }


    @Override
    public void onDetach() {
        super.onDetach();
       // d.showAlertDialogExit(getActivity(), "Alert", "Do you want to exit?");
    }



}
