package com.t3.happysmile.happysmilesapp.ui.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.ErrorCallback;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.t3.happysmile.happysmilesapp.Exception.BaseException;
import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.model.UploadImageData;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class CameraActivity extends Activity implements Callback,
        OnClickListener {

    private SurfaceView surfaceView;
    private SurfaceHolder surfaceHolder;
    private Camera camera;
    private ImageView flipCamera;
    private ImageView flashCameraButton,story_camera,share_cancel;
    private ImageView captureImage,friendimagecamera;
    private int cameraId;
    private boolean flashmode = false;
    private int rotation;
    static boolean click=true;
    static boolean ok=false;
    static boolean switchCmaera=true;
    static boolean shareImage=false;
    Bitmap loadedImage = null;
    Bitmap rotatedBitmap = null;
    LinearLayout share_ll;
    SharedPreferences pref;
    SharedPreferences.Editor edit;
    public File imageFile;
    UploadImageData imageSet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_demo);
        // camera surface view created
        pref=getApplication().getSharedPreferences("MyPref",0);
        edit=pref.edit();
        imageSet=new UploadImageData();
        /*getActionBar().hide();*/
        cameraId = CameraInfo.CAMERA_FACING_BACK;
        flipCamera = (ImageView) findViewById(R.id.flipCamera);
        flashCameraButton = (ImageView) findViewById(R.id.story_camera);
        captureImage = (ImageView) findViewById(R.id.captureImage);
        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        friendimagecamera=(ImageView)findViewById(R.id.friendimagecamera);
        share_ll=(LinearLayout)findViewById(R.id.share_ll);
        share_cancel=(ImageView)findViewById(R.id.share_cancel);
        surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        flipCamera.setOnClickListener(this);
        captureImage.setOnClickListener(this);
        flashCameraButton.setOnClickListener(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (Camera.getNumberOfCameras() > 1) {
            flipCamera.setVisibility(View.VISIBLE);
        }
        /*if (!getBaseContext().getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_FLASH)) {
            flashCameraButton.setVisibility(View.GONE);
        }*/
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        if (!openCamera(CameraInfo.CAMERA_FACING_BACK))
        {
            alertCameraDialog();
        }

    }

    private boolean openCamera(int id) {
        boolean result = false;
        cameraId = id;
        releaseCamera();
        try {
            camera = Camera.open(cameraId);
        } catch (Exception e) {
            BaseException.run(CameraActivity.this, "Camera", "Error in Camera");
           // e.printStackTrace();
        }
        if (camera != null) {
            try {
                setUpCamera(camera);
                camera.setErrorCallback(new ErrorCallback() {

                    @Override
                    public void onError(int error, Camera camera) {

                    }
                });
                camera.setPreviewDisplay(surfaceHolder);
                camera.startPreview();
                result = true;
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
                releaseCamera();
            }
        }
        return result;
    }

    private void setUpCamera(Camera c) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);
        rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degree = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degree = 0;
                break;
            case Surface.ROTATION_90:
                degree = 90;
                break;
            case Surface.ROTATION_180:
                degree = 180;
                break;
            case Surface.ROTATION_270:
                degree = 270;
                break;

            default:
                break;
        }

        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            // frontFacing
            rotation = (info.orientation + degree) % 330;
            rotation = (360 - rotation) % 360;
        } else {
            // Back-facing
            rotation = (info.orientation - degree + 360) % 360;
        }
        c.setDisplayOrientation(rotation);
        Parameters params = c.getParameters();

        showFlashButton(params);

        List<String> focusModes = params.getSupportedFlashModes();
        if (focusModes != null) {
            if (focusModes
                    .contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                params.setFlashMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            }
        }

        params.setRotation(rotation);
    }

    private void showFlashButton(Parameters params) {
        boolean showFlash = (getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_CAMERA_FLASH) && params.getFlashMode() != null)
                && params.getSupportedFlashModes() != null
                && params.getSupportedFocusModes().size() > 1;

        flashCameraButton.setVisibility(showFlash ? View.VISIBLE
                : View.INVISIBLE);

    }

    private void releaseCamera() {
        try {
            if (camera != null) {
                camera.setPreviewCallback(null);
                camera.setErrorCallback(null);
                camera.stopPreview();
                camera.release();
                camera = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("error", e.toString());
            camera = null;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.story_camera:
//                flashOnButton();
               if(shareImage) {
                   Intent story = new Intent(CameraActivity.this, StoryActivity.class);
                   story.putExtra("imageSet",imageSet);
                   startActivity(story);
                   overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                   releaseCamera();
               }
                else
               {
                   Toast.makeText(getApplication(), "first take pic", Toast.LENGTH_SHORT).show();
               }
                break;
            case R.id.flipCamera:
                if(switchCmaera)
                {
                    flipCamera();
                }
                else
                {
                    if(shareImage)
                    {
                         share_ll.setVisibility(View.VISIBLE);
                    }
                }
                break;
            case R.id.captureImage:
                if(click)
                {
                    takeImage();
                }
                else
                {
                    if(ok)
                    {
                        //send image to server and move to other page
                        Intent task=new Intent(CameraActivity.this,TaskInterface.class);
                        startActivity(task);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        releaseCamera();
                    }
                }

                break;

            case R.id.share_cancel:
            share_ll.setVisibility(View.INVISIBLE);
                break ;
            default:
                break;
        }
    }

    private void takeImage() {
        click=false;


        camera.takePicture(null, null, new PictureCallback() {



            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                try {
                    // convert byte array into bitmap

                    loadedImage = BitmapFactory.decodeByteArray(data, 0,
                            data.length);

                    // rotate Image
                    Matrix rotateMatrix = new Matrix();
                    rotateMatrix.postRotate(rotation);
                    rotatedBitmap = Bitmap.createBitmap(loadedImage, 0, 0,
                            loadedImage.getWidth(), loadedImage.getHeight(),
                            rotateMatrix, false);
                    String state = Environment.getExternalStorageState();
                    File folder = null;
                    if (state.contains(Environment.MEDIA_MOUNTED)) {
                        folder = new File(Environment
                                .getExternalStorageDirectory() + "/100HappySmilesApp");
                    } else {
                        folder = new File(Environment
                                .getExternalStorageDirectory() + "/100HappySmilesApp");
                    }

                    boolean success = true;
                    if (!folder.exists()) {
                        success = folder.mkdirs();
                    }
                    if (success) {
                        java.util.Date date = new java.util.Date();
                        imageFile = new File(folder.getAbsolutePath()
                                + File.separator
                                + new Timestamp(date.getTime()).toString()
                                + "Image.jpg");
                        imageSet.setImage(imageFile);

            String imagepath=folder.getAbsolutePath()
                                    + File.separator
                                    + new Timestamp(date.getTime()).toString()
                                    + "Image.jpg";

                        edit.putString("imagePath",imagepath);
                        edit.commit();
                        ArrayList<UploadImageData> image=new ArrayList();
                        UploadImageData imge=new UploadImageData();
                        imge.setImage(imageFile);
                        imge.setMagePath(imagepath);
                        image.add(imge);
                       /*Toast.makeText(getApplicationContext(),imagepath+"FIlE"+folder.getAbsolutePath()
                                + File.separator
                                + new Timestamp(date.getTime()).toString()
                                + "Image.jpg",Toast.LENGTH_LONG).show();*/
                        releaseCamera();
                    } else {
                        Toast.makeText(getBaseContext(), "Image Not saved",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                    ByteArrayOutputStream ostream = new ByteArrayOutputStream();

                    // save image into gallery
                    rotatedBitmap.compress(CompressFormat.JPEG, 100, ostream);

                    FileOutputStream fout = new FileOutputStream(imageFile);
                    fout.write(ostream.toByteArray());
                    fout.close();
                    ContentValues values = new ContentValues();

                    values.put(Images.Media.DATE_TAKEN,
                            System.currentTimeMillis());
                    values.put(Images.Media.MIME_TYPE, "image/jpeg");
                    values.put(MediaStore.MediaColumns.DATA,
                            imageFile.getAbsolutePath());

                    CameraActivity.this.getContentResolver().insert(
                            Images.Media.EXTERNAL_CONTENT_URI, values);
                    friendimagecamera.setVisibility(View.VISIBLE);
                    flashCameraButton.setVisibility(View.VISIBLE);
                    captureImage.setImageResource(R.drawable.done_normal);
                    flipCamera.setImageResource(R.drawable.share_normal);
                    ok=true;
                    shareImage=true;
                    switchCmaera=false;
                    edit.putInt("subTask",2);
                    edit.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void flipCamera() {
        int id = (cameraId == CameraInfo.CAMERA_FACING_BACK ? CameraInfo.CAMERA_FACING_FRONT
                : CameraInfo.CAMERA_FACING_BACK);
        if (!openCamera(id)) {
            alertCameraDialog();
        }
    }

    private void alertCameraDialog() {
        AlertDialog.Builder dialog = createAlert(CameraActivity.this,"Camera info", "error to open camera");
        dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        dialog.show();
    }

    private Builder createAlert(Context context, String title, String message) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(
                new ContextThemeWrapper(context,
                        android.R.style.Theme_Holo_Light_Dialog));
        dialog.setIcon(R.drawable.ic_launcher);
        if (title != null)
            dialog.setTitle(title);
        else
            dialog.setTitle("Information");
        dialog.setMessage(message);
        dialog.setCancelable(false);
        return dialog;

    }

    private void flashOnButton() {
        /*if (camera != null) {
            try {
                Parameters param = camera.getParameters();
                param.setFlashMode(!flashmode ? Parameters.FLASH_MODE_TORCH
                        : Parameters.FLASH_MODE_OFF);
                camera.setParameters(param);
                flashmode = !flashmode;
            } catch (Exception e) {
                // TODO: handle exception
            }

        }*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent magicalintent = new Intent(
                CameraActivity.this,Path.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        magicalintent.putExtra("currentPage",0);
        startActivity(magicalintent);
      //  finish();
        overridePendingTransition(R.anim.slide_out_left, R.anim.slide_in_right);
        releaseCamera();
    }
}
