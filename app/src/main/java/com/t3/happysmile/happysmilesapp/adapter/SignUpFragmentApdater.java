package com.t3.happysmile.happysmilesapp.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.t3.happysmile.happysmilesapp.R;
import com.t3.happysmile.happysmilesapp.ui.fragment.SignupFragment1;
import com.t3.happysmile.happysmilesapp.ui.fragment.SignupFragment2;


public class SignUpFragmentApdater extends FragmentPagerAdapter implements IconPagerAdapter {
    protected static final String[] CONTENT = new String[] { "This", "Is" };

    SharedPreferences pref;
    SharedPreferences.Editor edit;

    protected static final int[] ICONS = new int[] {
            R.drawable.second_icon_signup,
            R.drawable.second_icon_signup,
    };

    Context context;

    private int mCount = CONTENT.length;

    public SignUpFragmentApdater(FragmentManager fm,Context context) {
        super(fm);
        this.context=context;
           }

    @Override
    public Fragment getItem(int position) {
//        return TestFragment.newInstance(CONTENT[position % CONTENT.length]);


        switch(position) {

            case 0:

                return new SignupFragment1().newInstance("1", "1");

            //  return new TaskInterface();

            case 1:

                return new SignupFragment2().newInstance("1", "1");

            }



        return null;

    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return SignUpFragmentApdater.CONTENT[position % CONTENT.length];
    }

    @Override
    public int getIconResId(int index)
    {
        return ICONS[index % ICONS.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}