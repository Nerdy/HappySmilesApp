package com.t3.happysmile.happysmilesapp.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;

public class UploadImageData implements Parcelable {
    public UploadImageData(Parcel in) {
        magePath = in.readString();
    }

    public static final Creator<UploadImageData> CREATOR = new Creator<UploadImageData>() {
        @Override
        public UploadImageData createFromParcel(Parcel in) {
            return new UploadImageData(in);
        }

        @Override
        public UploadImageData[] newArray(int size) {
            return new UploadImageData[size];
        }
    };

    public UploadImageData() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(magePath);
    }

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    File image;

    public String getMagePath() {
        return magePath;
    }

    public void setMagePath(String magePath) {
        this.magePath = magePath;
    }

    String magePath;
}
